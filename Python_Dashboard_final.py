# Authors : Marion Estoup and Solal Peiffer-Smadja
# E-mail : marion_110@hotmail.fr
# January 2021



######### Python Dashboard

#################################### DATAMANAGEMENT before dash app

# Needed Libraries 
import pandas as pd
import dash_core_components 
import dash
import dash_bootstrap_components as dbc
import plotly.express as px
import numpy as np
from dash import dcc
from dash import html
from dash.dependencies import Input, Output
from pycountry_convert import country_alpha2_to_country_name, country_name_to_country_alpha3
from dash_bootstrap_templates import ThemeSwitchAIO, template_from_url




################################################### Life expectancy file
# Open file about life expectancies data
# It's a dataframe so no need to create one
life_exp = pd.read_csv("C:/Users/mario/Downloads/Life-Expectancy-Data.csv")


################################################### Get to know the data in the file life_exp

# Number of rows
len(life_exp) # Result : 2938 rows

# Number of columns 
len(life_exp.columns) # Result : 22 columns

# What are columns data types
life_exp.dtypes
# Column Country has data type object
# Column Year has data type int64
# Column Status has data type object
# Column Life expectancy has data type float64
# Column Adult Mortality has data type float 64
# Column infant deaths has data type int64
# Column Alcohol has data type float64
# Column percentage expenditure has data type float64
# Column Hepatitis B has data type float 64
# Column Measles has data type int64
# Column BMI has data type float64
# Column under-five deaths has data type int64
# Column Polio has data type float64
# Column Total expenditure has data type float64
# Column Diphteria has data type float64
# Column HIV/AIDS has data type float64
# Column GDP has data type float64
# Column Population has data type float64
# Column thinness 1-18 years has data type float64
# Column thinness 5-9 years has data type float64
# Column Income composition of resources has data type float64
# Column Schooling has data type float64


# Rename columns to remove spaces or middle dashes in order to avoid problems later
life_exp.columns = ["Country", "Year", "Status", "Life_exp", "Adult_Mortality", "Infant_Deaths",
                    "Alcohol", "Percentage_expenditure", "Hepatitis_B", "Measles","BMI",
                    "Under_Five_Deaths", "Polio", "Total_Expenditure", "Diphteria", "HIV_AIDS",
                    "GDP", "Popoulation", "Thinness_1_18_years", "Thinness_5_9_years",
                    "Income_Compo_Of_Resources", "Schooling"]
                                    

# Create mean_life_exp dataframe to have mean of life expectancy worldwide
# We have to calculate the mean for all countries in year 2000, then same for year 2001 etc until year 2015
# We keep the results in a dataframe to be able to plot it
# To do so we group by the column Year so that it calculates the mean of life expectancy for each different years
mean_life_exp = life_exp.groupby('Year').mean().reset_index()



################################################################ Life expectancy by Continent file

# Open file Continent 
life_exp_continent = pd.read_csv("C:/Users/mario/Downloads/Continent.csv")

# Check how many unique values in continent column
life_exp_continent['continent'].unique() 
# AS for Asia, EU for Europe, nan, SA for south america, OC for oceania, unkown, AF for africa
# NA for north america is missing, AN for antartica is missing
# And we have values nan and unknown so we need to identify them

 
# Create life_exp_unknown data frame to store values where it's unknown in life_exp_continent
life_exp_unknown = life_exp_continent.loc[life_exp_continent['continent'] == 'Unknown']
# 112 rows not well classified so we have to add them in the corresponding continents

# Create life_exp_nan to store values where it's nan in life_exp_continent
life_exp_nan =  life_exp_continent[life_exp_continent['continent'].isna()]
# 338 rows not well classified so we have to add them in the corresponding continents


# Calculate the mean of Life_expectancy for each continent between 2000 and 2015
# We store the result in life_exp_continent_year
life_exp_continent_year = life_exp_continent.groupby(['continent','year']).mean().reset_index()

# Create new column full_continent in life_exp_continent_year to replace continent codes by their corresponding full name 
life_exp_continent_year['full_continent'] = life_exp_continent_year['continent'].replace(['AF','AS','OC','EU', 'SA'],
                                                                                         ['Africa','Asia','Oceania','Europe', 'South America']) 

# Remove rows where it's unknown in the column continent of life_exp_continent_year
life_exp_continent_year = life_exp_continent_year[life_exp_continent_year["continent"] != "Unknown"]



# Create life_exp_continent2 based on life_exp_continent without unknown and nan values in the column code 
life_exp_continent2 = life_exp_continent[life_exp_continent['code'] != 'Unknown']
life_exp_continent2 = life_exp_continent2[life_exp_continent2['code'].notna()]

# Create the column code in life_exp_continent2 to have code iso for continents based on their names
# To do so we use the functions country_name_to_country_alpha3 and country_alpha2_to_country_name
life_exp_continent2['code'] = life_exp_continent2.code.apply(lambda x: country_name_to_country_alpha3(country_alpha2_to_country_name(x)))

# Create data frame grouped based on life_exp_continent2 which is grouped by code and country
grouped = life_exp_continent2.groupby(["code","country"])

# Create mean_infant_deaths based on grouped data frame 
mean_infant_deaths = grouped["infant_deaths"].sum() / grouped["year"].nunique()
mean_infant_deaths = mean_infant_deaths.rename("mean_infant_deaths")

# Create a data frame df_mean_infant_deaths based on mean_infant_deaths series
df_mean_infant_deaths = pd.DataFrame(mean_infant_deaths)
df_mean_infant_deaths = df_mean_infant_deaths.reset_index()

# Create mean_adult_mortality based on grouped in the same way as infant_deaths just before
mean_adult_mortality = grouped["adult_mortality"].sum() / grouped["year"].nunique()
mean_adult_mortality = mean_adult_mortality.rename("mean_adult_mortality")

# Create a data frame df_mean_adult_mortality based on mean_adult_mortality series
df_mean_adult_mortality = pd.DataFrame(mean_adult_mortality)
df_mean_adult_mortality = df_mean_adult_mortality.reset_index()

# Create mean_life_exp_country based on grouped in the same way as infant and adult mortality just before
mean_life_exp_country = grouped["life_expectancy"].sum() / grouped["year"].nunique()
mean_life_exp_country = mean_life_exp_country.rename("mean_life_exp")

# Create a data frame df_mean_life_exp_country based on mean_life_exp_country series
df_mean_life_exp_country = pd.DataFrame(mean_life_exp_country)
df_mean_life_exp_country = df_mean_life_exp_country.reset_index()

# Modify life_exp_continent by keeping rows where it's not unknown nor nan in continent column
life_exp_continent = life_exp_continent[life_exp_continent['continent'] != 'Unknown']
life_exp_continent = life_exp_continent[life_exp_continent['continent'].notna()]


################################################################# Infant and adult mortality file

# Creating data frames for infant and adult mortality based on columns in mean_life_exp data frame
# Creating df_adult with column year and adult mortality from mean_life_exp
df_adult = mean_life_exp[['Year', 'Adult_Mortality']]

# Creating df_infant with column year and infant deaths from mean_life_exp
df_infant = mean_life_exp[['Year', 'Infant_Deaths']]



################################################ Life Expectancy file for developed and developing countries

# how many unique/different values in Status column
life_exp['Status'].unique()  # Result is Developing and Developed

# Creation of life_exp_developed data frame
life_exp_developed = life_exp.loc[life_exp['Status'] == "Developed"]

# Creation of life_exp_developing dataa frame
life_exp_developing = life_exp.loc[life_exp['Status'] == "Developing"]

# Create mean_life_exp_developed dataframe to have mean of life expectancy worldwide for developed countries
# We have to calculate the mean for all countries in year 2000, then same for year 2001 etc until year 2015
# We keep the results in a dataframe to be able to plot it
# To do so we group by the column Year so that it calculates the mean of life expectancy for each different years
mean_life_exp_developed = life_exp_developed.groupby('Year').mean().reset_index()

# Create mean_life_exp_developing dataframe to have mean of life expectancy worldwide for developed countries
# We have to calculate the mean for all countries in year 2000, then same for year 2001 etc until year 2015
# We keep the results in a dataframe to be able to plot it
# To do so we group by the column Year so that it calculates the mean of life expectancy for each different years
mean_life_exp_developing = life_exp_developing.groupby('Year').mean().reset_index()

# Create a list for Life_exp_developed column
life_exp_developed["Life_exp_developed"] = list(mean_life_exp_developed.Life_exp) * 32

# Create a list for Life_exp_developing column
life_exp_developed["Life_exp_developing"] = list(mean_life_exp_developing.Life_exp) * 32

### We create new columns in mean_life_exp dataframe in order to have everything in only one dataframe instead of multiple ones

## Adult mortality for developed and developing countries
# Create column Adult_Mortality_developed_countries based on the column in mean_life_exp_developed data frame
mean_life_exp['Adult_Mortality_developed_countries'] = mean_life_exp_developed['Adult_Mortality']
# Create column Adult_Mortality_developing_countries based on the column in mean_life_exp_developing data frame
mean_life_exp['Adult_Mortality_developing_countries'] = mean_life_exp_developing['Adult_Mortality']

## Infant mortality for developed and developing countries
# Create column Infant_Mortality_developed_countries based on the column in mean_life_exp_developed data frame
mean_life_exp['Infant_Mortality_developed_countries'] = mean_life_exp_developed['Infant_Deaths']
# Create column Infant_Mortality_developing_countries based on the column in mean_life_exp_developing data frame
mean_life_exp['Infant_Mortality_developing_countries'] = mean_life_exp_developing['Infant_Deaths']

## Hepatitis B for developed and developing countries
# Create column Hepatitis_B_developed_countries based on the column in mean_life_exp_developed data frame
mean_life_exp['Hepatitis_B_developed_countries'] = mean_life_exp_developed['Hepatitis_B']
# Create column Hepatitis_B_developing_countries based on the column in mean_life_exp_developing data frame
mean_life_exp['Hepatitis_B_developing_countries'] = mean_life_exp_developing['Hepatitis_B']

## Polio for developed and developing countries
# Create column Polio_developed_countries based on the column in mean_life_exp_developed data frame
mean_life_exp['Polio_developed_countries'] = mean_life_exp_developed['Polio']
# Create column Polio_developing_countries based on the column in mean_life_exp_developing data frame
mean_life_exp['Polio_developing_countries'] = mean_life_exp_developing['Polio']

## Diphteria for developed and developing countries
# Create column Diphteria_developed_countries based on the column in mean_life_exp_developed data frame
mean_life_exp['Diphteria_developed_countries'] = mean_life_exp_developed['Diphteria']
# Create column Diphteria_developing_countries based on the column in mean_life_exp_developing data frame
mean_life_exp['Diphteria_developing_countries'] = mean_life_exp_developing['Diphteria']

## Alcohol for developed and developing countries
# Create column Alcohol_developed_countries based on the column in mean_life_exp_developed data frame
mean_life_exp['Alcohol_developed_countries'] = mean_life_exp_developed['Alcohol']
# Create column Alcohol_developing_countries based on the column in mean_life_exp_developing data frame
mean_life_exp['Alcohol_developing_countries'] = mean_life_exp_developing['Alcohol']

## Measles for developed and developing countries
# Create column Measles_developed_countries based on the column in mean_life_exp_developed data frame
mean_life_exp['Measles_developed_countries'] = mean_life_exp_developed['Measles']
# Create column Measles_developing_countries based on the column in mean_life_exp_developing data frame
mean_life_exp['Measles_developing_countries'] = mean_life_exp_developing['Measles']

## Life expectancy for developed and developing countries
# Create column Life_exp_developed based on the column in mean_life_exp_developed data frame
mean_life_exp['Life_exp_developed'] = mean_life_exp_developed['Life_exp']
# Create column Life_exp_developing based on the column in mean_life_exp_developing data frame
mean_life_exp['Life_exp_developing'] = mean_life_exp_developing['Life_exp']



################################################### DASHBOARD

# Two different templates stored in variables for the dash app
template_theme1 = "quartz"
template_theme2 = "vapor"
url_theme1 = dbc.themes.QUARTZ
url_theme2 = dbc.themes.VAPOR

# Link which gives access to the templates
dbc_css = (
    "https://cdn.jsdelivr.net/gh/AnnMarieW/dash-bootstrap-templates@V1.0.1/dbc.min.css"
)


## Creating the app 

# Create the app and define the main theme in external_stylesheets
app = dash.Dash(__name__, external_stylesheets=[url_theme1, dbc_css])

# Define a sentence that appears on the top left of the app in order to choose between the two proposed themes
header = html.P("Click if you want to change theme of graphics",style={'textAlign': 'left','color': 'white', 'font-size':'300','font-family':'verdana'})


# Create the layout of the app 
app.layout = html.Div([ # html Div to use html components (style, text, tabs, subtabs etc)
    html.Div(
        html.H1('Life expectancy dashboard', style={'textAlign': 'center','color': 'white', 'font-size':'300','font-family':'verdana'}) # Main title Life expectancy dashboard and its style
    ),
    dbc.Container( # dbc container and row to use dbc Col to define the different themes to choose
        dbc.Row(
            [
                dbc.Col( # Define the two possible themes to choose
                    [
                        header, # header is the sentence that we've defined earlier in header variable
                        ThemeSwitchAIO( # switch with icons
                            aio_id="theme", # id of the switch
                            themes=[url_theme1,url_theme2], # proposed themes that we've defined earlier
                            ),
                        ])])),
    
    dcc.Tabs(id='tabs', value='Tab1', children=[ # Create tabs and their sub tabs
        dcc.Tab(label='Worldwide life expectancy', id='tab1', value='Tab1', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'},children =[ # First tab called Worldwide life expectancy
            dcc.Tabs(id="subtabs1", value='Subtab1',children=[ # Create sub tabs in the first tab Worldwide life expectancy
                dcc.Tab(label='Life Expectancy Worldwide', id='subtab1', value='Subtab1',style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # First sub tab of tab 1
                dcc.Tab(label='Life Expectancy Worldwide By Countries', id='subtab2', value='Subtab2',style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Second sub tab of tab 1
                dcc.Tab(label='Life Expectancy Worldwide By Continents', id='subtab3', value='Subtab3',style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Third sub tab of tab 1
                dcc.Tab(label='Life Expectancy Worldwide (Map)', id='subtab41', value='Subtab41',style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'})]) # Fourth sub tab of tab 1
        ]),
        
        
        dcc.Tab(label=' Worldwide mortality', id='tab2', value='Tab2',style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}, children=[ # Create the second tab called Worldwide mortality
            dcc.Tabs(id="subtabs4", value='Subtab4', children=[ # Create sub tabs for the second tab
                dcc.Tab(label='Adult Death and Infant death Worldwide', id='subtab4', value='Subtab4', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # First sub tab of tab 2
                dcc.Tab(label='Adult Death and Infant death By Countries', id='subtab5', value='Subtab5', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Second sub tab of tab 2
                dcc.Tab(label='Adult Death and Infant death By Continents', id='subtab6', value='Subtab6', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Third sub tab of tab 2
                dcc.Tab(label='Adult Death (Map)', id='subtab7', value='Subtab7', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Fourth sub tab of tab 2
                dcc.Tab(label='Infant Death (Map)', id='subtab42', value='Subtab42', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'})]) # Fifth sub tab of tab 2
            ]),
        
        
        dcc.Tab(label=' Worldwide health', id='tab3', value='Tab3', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'},children=[ # Create the third tab called Worldwide health
            dcc.Tabs(id="subtabs8", value='Subtab8', children=[ # Create sub tabs for the third tab
                dcc.Tab(label='Worldwide Immunization Coverage', id='subtab8', value='Subtab8', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the first sub tab of the third tab 
                dcc.Tab(label='Worldwide Immunization Coverage By Countries', id='subtab9', value='Subtab9', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the second sub tab of the third tab                         
                dcc.Tab(label='Worldwide Immunization Coverage By Continents', id='subtab10', value='Subtab10', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the third sub tab of the third tab                                           
                dcc.Tab(label='Worldwide Alcohol Consumption', id='subtab11', value='Subtab11', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the fourth sub tab of the third tab
                dcc.Tab(label='Worldwide Alcohol Consumption By Countries', id='subtab12', value='Subtab12', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the fifth sub tab of the third tab
                dcc.Tab(label='Worldwide Alcohol Consumption By Continents', id='subtab13', value='Subtab13', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the sixth sub tab of the third tab
                dcc.Tab(label='Worldwide Reported Cases having Measles', id='subtab14', value='Subtab14', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the seventh sub tab of the third tab
                dcc.Tab(label='Worldwide Reported Cases having Measles By Countries', id='subtab15', value='Subtab15', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the eigth sub tab of the third tab
                dcc.Tab(label='Worldwide Reported Cases having Measles By Continents', id='subtab16', value='Subtab16', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'})]), # Create the nineth sub tab of the third tab 
            ]),
        
       
        
        dcc.Tab(label='Worldwide life expectancy between developed and developing countries', id='tab4', value='Tab4', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'},children=[ # Create the fourth tab called Worldwide life expectancy between developed and developing countries
                                                                                              
            dcc.Tabs(id="subtabs17", value='Subtab17', children=[ # Create sub tabs for the fourth tab 
                dcc.Tab(label='Life Expectancy Worldwide', id='subtab17', value='Subtab17', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the first sub tab of the fourth tab
                dcc.Tab(label='Life Expectancy Worldwide By Developed Countries', id='subtab18', value='Subtab18', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the second sub tab of the fourth tab
                dcc.Tab(label='Life Expectancy Worldwide By Developing Countries', id='subtab19', value='Subtab19', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'})]), # Create the third sub tab of the fourth tab
            ]),
        
        dcc.Tab(label='Worldwide mortality between developed and developing countries', id='tab5', value='Tab5', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}, children=[ # Create the fifth tab called Worldwide mortality between developed and developing countries
            
            dcc.Tabs(id="subtabs20", value="Subtab20", children=[ # Create sub tabs for the fifth tab
                dcc.Tab(label='Mean Adult Death Worldwide ', id='subtab20', value='Subtab20', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the first sub tab of the fifth tab
                dcc.Tab(label='Mean Adult Death Worldwide By Developed Countries', id='subtab21', value='Subtab21', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the second sub tab of the fifth tab
                dcc.Tab(label='Mean Adult Death Worldwide By Developing Countries', id='subtab22', value='Subtab22', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the third sub tab of the fifth tab
                dcc.Tab(label='Mean Infant Death Worldwide ', id='subtab23', value='Subtab23', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the fourth sub tab of the fifth tab
                dcc.Tab(label='Mean Infant Death Worldwide By Developed Countries', id='subtab24', value='Subtab24', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the fifth sub tab of the fifth tab
                dcc.Tab(label='Mean Infant Death Worldwide By Developing Countries', id='subtab25', value='Subtab25', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'})]), # Create the sixth sub tab of the fifth tab
            ]),
        
        dcc.Tab(label='Worldwide health for Developed Countries', id='tab6', value='Tab6',style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}, children=[ # Create the sixth tab called Worldwide health for Developed Countries
            
            dcc.Tabs(id="subtabs26", value="Subtab26", children=[ # Create sub tabs for the sixth tab
                dcc.Tab(label='Worldwide Immunization Coverage (hepatitis b)', id='subtab26',value='Subtab26', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the first sub tab of the sixth tab
                dcc.Tab(label='Worldwide Immunization Coverage By Developed Countries (hepatitis b)', id='subtab27',value='Subtab27', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the second sub tab of the sixth tab
                dcc.Tab(label='Worldwide Immunization Coverage  (polio)', id='subtab28',value='Subtab28', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the third sub tab of the sixth tab
                dcc.Tab(label='Worldwide Immunization Coverage By Developed Countries (polio)', id='subtab29',value='Subtab29', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the fourth sub tab of the sixth tab
                dcc.Tab(label='Worldwide Immunization Coverage  (diphteria)', id='subtab30',value='Subtab30', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the fifth sub tab of the sixth tab
                dcc.Tab(label='Worldwide Immunization Coverage By Developed Countries (diphteria)', id='subtab31',value='Subtab31', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the sixth sub tab of the sixth tab
                dcc.Tab(label='Worldwide Alcohol Consumption', id='subtab32',value='Subtab32', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the seventh sub tab of the sixth tab
                dcc.Tab(label='Worldwide Alcohol Consumption By Developed Countries', id='subtab33',value='Subtab33', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the eigth sub tab of the sixth tab
                dcc.Tab(label='Worldwide Reported Cases having Measles ', id='subtab34', value='Subtab34', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the nineth sub tab of the sixth tab
                dcc.Tab(label='Worldwide Reported Cases having Measles By Developed Countries ', id='subtab35', value='Subtab35', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'})]), # Create the tenth sub tab of the sixth tab
            ]),


        dcc.Tab(label='Worldwide health for Developing Countries', id='tab7',value='Tab7', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'},children=[ # Create the seventh tab called Worldwide health for Developing Countries
            
            dcc.Tabs(id='subtabs36',value='Subtab36', children=[ # Create sub tabs for the seventh tab
                dcc.Tab(label='Worldwide Immunization Coverage By Developing Countries (hepatitis b)', id='subtab36',value='Subtab36', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # Create the first sub tab of the seventh tab
                dcc.Tab(label='Worldwide Immunization Coverage By Developing Countries (polio)', id='subtab37',value='Subtab37', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the second sub tab of the seventh tab
                dcc.Tab(label='Worldwide Immunization Coverage By Developing Countries (diphteria)', id='subtab38',value='Subtab38', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the third sub tab of the seventh tab
                dcc.Tab(label='Worldwide Alcohol Consumption By Developing Countries ', id='subtab39',value='Subtab39', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Create the fourth sub tab of the seventh tab
                dcc.Tab(label='Worldwide Reported Cases having Measles By Developing Countries', id='subtab40', value='Subtab40', style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'})]), # Create the fifth sub tab of the seventh tab
            ])
        
        ])
    ])



############################################ Tab 1
############### Sub tab 1 Life Expectancy Worldwide 

# Callback for Tab 1 and subtab 1 Life Expectancy Worldwide 
@app.callback(Output('subtab1', 'children'), # Output (what we see) is the content of the first subtab in tab 1
              [Input('subtabs1', 'value'), # Input (what we give) is the content of the different subtabs that we choose in tab 1
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected

# Function to display a graph in the first subtab in tab 1
def display_graph_subtab1(value, toggle):
    
    if value == 'Subtab1':# If we are in subtab1 in tab 1 we return a graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected
        return html.Div([ # html components
            html.Div(
                html.P("Mean life expectancy worldwide between 2000 and 2015", # Graph title
                       style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # Graph style
                ),
            dcc.Graph (id="Mean_life_exp_line_plot", # Id of the graph
                       figure = px.line(mean_life_exp, # Figure variable that contains the graph
                                        x='Year',  # x axis
                                        y='Life_exp', # y axis
                                        labels={'Year':'Years', # name of x axis
                                                'Life_exp':'Life Expectancy'}, # name of y axis
                                        markers=True, # to have points on the line
                                        template=template))]) # selected template





   

############################################ Tab 1
############### Sub tab 2 Life Expectancy Worldwide By Countries 

# Callback for Tab 1 and subtab 2 Life Expectancy Worldwide By Countries 
@app.callback(Output('subtab2', 'children'), # Output is the content of the second sub tab in tab 1
              [Input('subtabs1', 'value'),  # Input is the content of the different sub tabs that we choose in tab 1
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected 

# Function to create a dropdown menu and radio items 
# (different countries that we can choose to see their life expectancy and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    
    if value == 'Subtab2': # If we are in subtab2 in tab 1 we return a graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected
        return html.Div([ # html components
                    html.P("Choose the country where you want to see the mean life expectancy between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Style of the sentence
                    dcc.Dropdown(id='worldwide_dropdown', # Create dropdown (list with all countries to choose the one where we want to see the mean life expectancy between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp data frame
                    placeholder= 'Select the country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Style of the sentence within the dropdown
                    html.P("Choose the type of graph that you want to see", # Sentence that appears above the radio item selection
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Style of the sentence
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Style of the drop down
                    dcc.Graph(id="scatter-plot"), # dcc.Graph allows to create a graph
                              html.P("Mean life expectancy worldwide by country between 2000 and 2015", # Graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # Style of graph title
                                    
                    ]) 
   
    
# Callback for Tab 1 and sub tab 2 to display the graph (by countries)
@app.callback(
    Output("scatter-plot", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected

# Function to update the graph depending on which country we choose in the dropdown menu and which type of graph in the radio items
def update_graph_countries(dropdown, radio_items, toggle): 

    template = template_theme1 if toggle else template_theme2 # We define the theme selected

    mask = (life_exp.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp data frame

    if radio_items =='line': # If the user selects line in radio item we return a line chart
        fig = px.line(life_exp[mask], # We store the graph in fig variable
                x='Year', y='Life_exp', markers=True, # x and y axis and markers to have points on the line
                labels={'Year':'Years', # x axis name
                        'Life_exp':'Life Expectancy'}, # y axis name
                                        template=template) # template selected
        
        return fig      # We return the line plot 

    if radio_items == 'bar': # If the user selects bar in radio item we return a bar chart
        
        fig2 = px.bar(life_exp[mask], # We store the graph in fig2 variable
                x='Year', y='Life_exp', # x and y axis
                labels={'Year':'Years', # x axis name
                        'Life_exp':'Life Expectancy'}, # y axis name
                                        template=template)  # template selected
        return fig2 # We return the bar plot 
    



############################################ Tab 1
############### Sub tab 3 Life Expectancy Worldwide By Continents 

# Callback for Tab 1 and subtab 3 Life Expectancy Worldwide By Continents
@app.callback(Output('subtab3', 'children'), # Output is the content of subtab3 in tab 1
              [Input('subtabs1', 'value')]) # Input is the content of the different sub tabs that we choose in tab 1

# Function to create a dropdown menu and radioitems (different continents that we can choose to see their life expectancy and radio items to choose between bar chart or line chart)
def dropdown_continent(value): 
    if value == 'Subtab3': # If we are in subtab3 in tab 1 we return a dropdown and radioitems

        return html.Div([ # html components
                    html.P("Choose the continent where you want to see the mean life expectancy between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Style of the sentence
                    dcc.Dropdown(id='worldwide_dropdown_continent', # Create dropdown (list with all continents to choose the one where we want to see the mean life expectancy between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': continents, 'value': continents} for continents in life_exp_continent_year['full_continent'].unique()], # Names of the continents in the column full_continent of life_exp_continent_year will appear in the dropdownn list
                    placeholder= 'Select the continent', # Placeholder, text that appears within the dropdown
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Style of placeholder text
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # Style of the sentence
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # Style of radio items
                    dcc.Graph(id="scatter-plot-continent"), # Dcc.Graph allows to create a graph
                     html.P("Mean life expectancy worldwide by continent between 2000 and 2015", # Graph title
                            style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # Style of graph title
                     
                     ]) 


# Callback for Tab 1 and sub tab 3 to display the graph (by continents)
@app.callback(
    Output("scatter-plot-continent", "figure"), # Output, what we want to see is the graph called scatter-plot-continent
    [Input("worldwide_dropdown_continent", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown_continent
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected


# Function to update the graph depending on which continent we choose in the dropdown menu and which type of graph we choose in the radio items
def update_graph_continent(dropdown, radio_items, toggle): # Create a function to update the graph
    
    mask = (life_exp_continent_year.full_continent == dropdown) # The item selected in the dropdown corresponds to the value of continents that are in the column full_continent of life_exp_continent_year data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
     
    if radio_items == 'line': # If the user chooses the radio item line we return a line plot
        fig = px.line(life_exp_continent_year[mask],# We store the graph in fig variable
            x='year', y='life_expectancy', markers=True, # x and y axis and markers to have points on the line
            labels={'year':'Years', # x axis name
                    'life_expectancy':'Life Expectancy'}, # y axis name
                                        template=template) # template selected
            
        return fig # We return fig

    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar chart
        fig2 = px.bar(life_exp_continent_year[mask], # We store the graph in fig2 variable
                       x='year', y='life_expectancy', # x and y axis
                       labels={'year':'Years', # x axis name
                               'life_expectancy':'Life Expectancy'}, # y axis name
                                        template=template) # template selected
        return fig2 # We return fig2 


############################################ Tab 1
############### Sub tab 41 Life Expectancy Worldwide (Map) (fourth sub tab of tab 1)

# Call back for sub tab 41 (fourth sub tab of tab 1)
@app.callback(Output('subtab41', 'children'), # Output is the content of the second sub tab in tab 1
              [Input('subtabs1', 'value'), # Input is the content of the different sub tabs that we choose in tab 1
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to display a map in the fourth subtab in tab 1
def display_graph_subtab8(value, toggle): 
    if value == 'Subtab41': # If we are in sub tab 41 we return the map
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
            html.Div(
                html.P("Map of life expectancy worldwide between 2000 and 2015", # Map title
                       style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # Style of map title
                ),
            dcc.Graph(id='map-plot', # Dcc.Graph allows to create a graph
                      figure = px.choropleth(df_mean_life_exp_country, locations="code", # map stored in figure variable
                    color="mean_life_exp", # mean_life_exp column to fill the map
                    hover_name="country", # column country to add to hover information
                    color_continuous_scale=px.colors.sequential.Blues, # color of the map
                    template = template # selected template in the background
                    ))])
            

        
   
############################################ Tab 2
############### Sub tab 4 (first sub tab of tab 2) Adult and Infant death Worldwide

# Callback for subtab4 in tab 2 (first subtab of tab 2) Adult and Infant death Worldwide
@app.callback(Output('subtab4', 'children'), # Output is the content of subtab4 in tab 2
              [Input('subtabs4', 'value'),  # Input is the content of the different sub tabs that are in tab 2
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected

# Function to display a graph Adult and Infant death Worldwide
def display_graph_subtab4(value, toggle): 
    
    if value == 'Subtab4': # If we are in subtab4 in tab 2 we return a line chart
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
            html.Div(
                
                html.P("Mean of the adult and infant mortality worldwide between 2000 and 2015", # Graph title
                        style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # Style of graph title
                
                ),
            dcc.Graph (id="Mean_life_exp_line_plot", # Dcc.Graph allows to create a graph
                       
                       figure = px.line(mean_life_exp, # We store the graph in figure variable
                                        x='Year', # x axis
                                        y=['Adult_Mortality','Infant_Deaths'], # y axis
                                        labels={'Year':'Years', 'value':'Adult and Infant Mortality'}, # x and y axis names
                                        markers=True, # to have points on the line
                                        template=template))])  # template selected                   




  
############################################ Tab 2
############### Sub tab 5 (second sub tab of tab 2) Adult and Infant death By Countries 

# Callback for Tab 2 and subtab 5 Adult and Infant death By Countries 
@app.callback(Output('subtab5', 'children'), # Output is the content of the second sub tab in tab 2
              [Input('subtabs4', 'value'),  # Input is the content of the different sub tabs that we choose in tab 2
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected

# Function to create a dropdown menu and radio items (different countries that we can choose to see their adult and infant mortality and radio items to choose bar chart or line chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab5': # If we are in subtab5 in tab 2 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the country where you want to see the mean of adult and infant mortality between 2000 and 2015",# Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),# style of text
                    dcc.Dropdown(id='worldwide_dropdown_mortality_country', # Create dropdown (list with all countries to choose the one where we want to see the mean adult and infant deaths between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp['Country'].unique()], # the dropdown list contains each unique countries that are stored in the column Country of life_exp data frame
                    placeholder= 'Select a country', # Placeholder, text that appears within the dropdown
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder text
                    html.P("Choose the type of graph that you want to see", # Sentence above the radio items
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of radio item
                    
                    dcc.Graph(id="scatter-plot-mortality-country"), # dcc.Graph allows to create a graph
                    html.P("Mean of the adult and infant mortality worldwide by country between 2000 and 2015", # Graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'})# Style of the graph title
                    ]) 
  
# Callback for Tab 2 and sub tab 5 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-mortality-country", "figure"), # Output is the scatter-plot-mortality-country
    [Input("worldwide_dropdown_mortality_country","value")], # Input is the data from the list in the dropdown called worldwide_dropdown_mortality_country
    [Input('radio_items_type_graph', 'value'), # Another input is the selection that the user does in the radio items selection
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected

# Function to update the graph depending on which country we choose in the dropdown menu and depending on which type of graph we choose in the radio items
def update_graph_countries_mortality(dropdown, radio_items, toggle): 
    
    mask = (life_exp.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    if radio_items =='line': # If the user selects line in radio item we return a line chart
        fig = px.line(life_exp[mask], # We store the graph in fig variable
                      
                      x='Year', # x axis
                      y=["Adult_Mortality","Infant_Deaths"], # y axis
                      labels={'Year':'Years', 'value':'Adult and Infant Mortality'}, # x and y axis names
                      markers=True, # markers to have points on the line
                                        template=template) # template selected
        return fig # We return the line plot 
    
    if radio_items =='bar': # If the user selects bar in radio item we return a bar chart
        fig2 = px.bar(life_exp[mask], # We store the graph in fig2 variable
                      x='Year', # x axis
                      y=['Adult_Mortality','Infant_Deaths'], # y axis
                      labels={'Year':'Years', 'value':'Adult and Infant Mortality'},# x and y axis names
                                        template=template # template selected
)
        return fig2 # We return the bar plot 
    
 
    
       
 
############################################ Tab 2
############### Sub tab 6 (third sub tab of tab 2) Adult and Infant death By Continents 

# Callback for Tab 2 and subtab 6 Adult and Infant death By Continents 
@app.callback(Output('subtab6', 'children'), # Output is the content of the third sub tab in tab 2
              [Input('subtabs4', 'value'), # Input is the content of the different sub tabs that we choose in tab 2
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected

# Function to create a dropdown menu and radio items (different continents that we can choose to see their adult and infant mortality and radio items to choose between bar chart or line chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab6': # If we are in subtab6 in tab 2 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the continent where you want to see the mean of adult and infant mortality between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_mortality_continent', # Create dropdown (list with all continents to choose the one where we want to see the mean adult and infant mortality between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': continents, 'value': continents} for continents in life_exp_continent_year['full_continent'].unique()], # the dropdown list contains each unique continents that are stored in the column full_continent of life_exp_continent_year data frame
                    placeholder= 'Select a continent', # Placeholder, text that appears within the dropdown
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), #  style of text

                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio items
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                    
                    dcc.Graph(id="scatter-plot-mortality-continent"), # dcc.Graph allows to create a graph
                    html.P("Mean of the adult and infant mortality worldwide by continent between 2000 and 2015", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'})# Style of graph title
                    ]) 


# Callback for Tab 2 and sub tab 6 to display the graph (by continents)
@app.callback(
    Output("scatter-plot-mortality-continent", "figure"), # Output is the scatter-plot-mortality-continent
    [Input("worldwide_dropdown_mortality_continent", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown_mortality_continent
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the type of graph in radio items
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected

# Function to update the graph depending on which country we choose in the dropdown menu and which graph we choose in the radio items
def update_graph_continent_mortality(dropdown, radio_items, toggle): 
    
    mask = (life_exp_continent_year.full_continent == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    if radio_items =='line': # If the user selects line in radio item we return a line chart
        
        fig = px.line(life_exp_continent_year[mask], # We store the graph in fig variable
                      
                      x='year', # x axis
                      y=["adult_mortality","infant_deaths"], # y axis
                      labels={'year':'Years', 'value':'Adult and Infant Mortality'}, # x and y axis names
                      markers=True, # markers to have points on the line
                                        template=template) # template selected
        return fig # We return the line plot 
    
    if radio_items =='bar': # If the user selects bar in radio item we return a bar chart
        
        fig2 = px.bar(life_exp_continent_year[mask], # We store the graph in fig2 variable
                     
                     x='year', # x axis
                     y=["adult_mortality","infant_deaths"], # y axis
                      labels={'year':'Years', 'value':'Adult and Infant Mortality'}, # x and y axis names
                                        template=template)     # template selected    
        return fig2 # If the user selects the radio item bar chart we return a bar chart
 
    
    

   

############################################ Tab 2
############### Sub tab 7 Adult Death Worldwide (Map) (fourth sub tab of tab 2)


# Call back for sub tab 7 (fourth sub tab of tab 2)
@app.callback(Output('subtab7', 'children'), # Output is the content of the fourth sub tab in tab 2
              [Input('subtabs4', 'value'), # Input is the content of the different sub tabs that we choose in tab 2
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected  

def display_graph_subtab8(value, toggle): # Definition of the function
    if value == 'Subtab7': # If we are in sub tab 7 we return the map
        template = template_theme1 if toggle else template_theme2  # We define the theme selected

        return html.Div([ # html components
            html.Div(
                html.P("Map of Adult Death Worldwide between 2000 and 2015", # map title
                       style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of map title
                ),
            dcc.Graph(id='map-plot', # dcc.graph allows to create a graph
                      figure = px.choropleth(df_mean_adult_mortality, locations="code", # we store the map in figure variable
                    color="mean_adult_mortality", # column to fill the map
                    hover_name="country", # column to add to hover information
                    color_continuous_scale=px.colors.sequential.Plasma, # fill color
                    template = template))]) # selected template


############################################ Tab 2
############### Sub tab 42 Infant Death Worldwide (Map) (fifth sub tab of tab 2)

# Call back for sub tab 42 (fifth sub tab of tab 5)
@app.callback(Output('subtab42', 'children'), # Output is the content of the second sub tab in tab 1
              [Input('subtabs4', 'value'), # Input is the content of the different sub tabs that we choose in tab 2
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])   # Another input is the theme selected

def display_graph_subtab8(value, toggle): # Definition of the function
    if value == 'Subtab42': # If we are in sub tab 42 we return the graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
            html.Div(
                html.P("Map of Adult Death Worldwide between 2000 and 2015", # map title
                       style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of map title
                ),
            dcc.Graph(id='map-plot', # dcc.graph allows to create a graph
                      figure = px.choropleth(df_mean_infant_deaths, locations="code",  # we store the map in figure variable
                    color="mean_infant_deaths", # column to fill the map
                    hover_name="country", # column to add to hover information
                    color_continuous_scale=px.colors.sequential.Plasma, # fill color
                    template = template))])  # selected template




    
############################################ Tab 3
############### Sub tab 8 (first sub tab of tab 3) Worldwide Immunization Coverage 

# Callback for sub tab 8 in tab 3 (first subtab of tab 3) Worldwide Immunization Coverage 
@app.callback(Output('subtab8', 'children'), # Output is the content of subtab8 in tab 3
              [Input('subtabs8', 'value'), # Input is the content of the different sub tabs that we choose in tab 3
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected


# Function to display the graph in sub tab 8 tab 3
def display_graph_subtab8(value, toggle): # Definition of the function
    if value == 'Subtab8': # If we are in sub tab 8 we return the graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
            html.Div(
                html.P("Mean of the Worldwide Immunization Coverage among 1-year-olds for Hepatitis B, Polio and Diphteria between 2000 and 2015 (%)", # graph title
                       style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
                ),
            dcc.Graph(id='Mean_immunization_line_plot', # dcc.graph allows to create a graph
                      figure = px.line(mean_life_exp, # we store the graph in figure variable
                                       x='Year', # x axis
                                       y=['Hepatitis_B','Polio','Diphteria'], # y axis
                                       labels={'Year':'Years', 'value':'Immunization Coverage (%)'}, # x and y axis names

                                       markers=True, # to have points on the line
                                       template=template))]) # selected template
            
            
    



############################################ Tab 3
############### Sub tab 9 (second sub tab of tab 3) Worldwide Immunization Coverage By Countries
# Callback for Tab 3 and subtab 9 Worldwide Immunization Coverage  By Countries
@app.callback(Output('subtab9', 'children'), # Output is the content of the second sub tab in tab 3
              [Input('subtabs8', 'value'),  # Input is the content of the different sub tabs that we choose in tab 3
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected  

# Function to create a dropdown menu and radio items (different countries that we can choose to see their immunization coverage concerning hepatitis b,Polio, diphteria and radio items to choose bar chart or line chart)
def dropdown_hpd_countries(value, toggle):  # Definition of the function
    if value == 'Subtab9': # If we are in subtab 9 in tab 3 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([# html components
                    html.P("Choose the country where you want to see the mean of the worldwide immunization coverage concerning hepatitis b, Polio and diphteria between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_hpd_country', # Create dropdown (list with all countries to choose the one where we want to see the mean of the worldwide immunization coverage between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp['Country'].unique()], # the dropdown list contains each unique countries that are stored in the column Country of life_exp data frame
                    placeholder= 'Select a country',# Placeholder, text that appears within the dropdown
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # sentence above radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}) # style of radio item
                    ,
                    dcc.Graph(id="scatter-plot-hpd-country"), # dcc.graph allows to create a graph
                    html.P("Mean of the Worldwide Immunization Coverage among 1-year-olds by country for Hepatitis B, Polio and Diphteria between 2000 and 2015 (%)", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
                    ]) 

 
    
   
# Callback for Tab 3 and sub tab 9 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-hpd-country", "figure"), # Output is the scatter-plot-hpd-country
    [Input("worldwide_dropdown_hpd_country", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown_hpd_country
    [Input('radio_items_type_graph', 'value'), # Another input is the type of graph that we choose in the radio items
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to update the graph depending on which country we choose in the dropdown menu and which type of graph we choose in the radio items
def update_graph_countries_mortality(dropdown, radio_items, toggle): # Definition of the function to update the graph
    
    mask = (life_exp.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp data frame
    template = template_theme1 if toggle else template_theme # We define the theme selected
    if radio_items =='line': # If the user selects line in radio item we return a line chart
        
        fig = px.line(life_exp[mask], # We store the graph in fig variable
                      x='Year', # x axis
                      y=['Hepatitis_B','Polio','Diphteria'], # y axis
                      labels={'Year':'Years', 'value':'Immunization Coverage (%)'}, # x and y axis names
                      markers=True, # markers to have points on the line
                      template=template) # template selected
        return fig # We return the line plot 
    
    if radio_items == 'bar': # If the user selects bar in radio item we return a bar chart
        fig2 = px.bar(life_exp[mask], # We store the graph in fig2 variable
                      x='Year', # x axis
                      y=['Hepatitis_B','Polio','Diphteria'], # y axis
                      labels={'Year':'Years', 'value':'Immunization Coverage (%)'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template # template selected 
)   
        return fig2 # we return the bar chart
        



############################################ Tab 3
############### Sub tab 10 (third sub tab of tab 3) Worldwide Immunization Coverage by continent 


# Callback for Tab 3 and subtab 10 Worldwide Immunization Coverage by continent 
@app.callback(Output('subtab10', 'children'), # Output is the content of the third sub tab in tab 3
              [Input('subtabs8', 'value'), # Input is the content of the different sub tabs that we choose in tab 3
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to create a dropdown menu and radio items (different continents that we can choose to see their Immunization Coverage among 1-year-olds for Hepatitis B, Polio and Diphteria between 2000 and 2015)
def dropdown_hpd_continent(value, toggle):  # Definition of the function
    if value == 'Subtab10': # If we are in sub tab 10 in tab 3 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the continent where you want to see the mean of the worldwide immunization coverage concerning hepatitis b, Polio and diphteria between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # style of text
                    dcc.Dropdown(id='worldwide_dropdown_hpd_continent', # Create dropdown (list with all continents to choose the one where we want to see the mean of the worldwide immunization coverage concerning hepatitis b, Polio and diphteria between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': continents, 'value': continents} for continents in life_exp_continent_year['full_continent'].unique()], # the dropdown list contains each unique continents that are stored in the column full_continent of life_exp_continent_year data frame
                    placeholder= 'Select a continent', # Placeholder, text that appears within the dropdown
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of radio item
                    
                    dcc.Graph(id="scatter-plot-hpd-continent"), # dcc.graph allows to create a graph
                    html.P("Mean of the Worldwide Immunization Coverage among 1-year-olds by continent for Hepatitis B, Polio and Diphteria between 2000 and 2015 (%)", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'})# Style of graph title
                    ]) 



  
# Callback for Tab 3 and sub tab 10 to display the graph (by continents)
@app.callback(
    Output("scatter-plot-hpd-continent", "figure"), # Output is the scatter-plot-hpd-continent
    [Input("worldwide_dropdown_hpd_continent", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown_hpd_continent
    [Input('radio_items_type_graph', 'value'), # Another input is the type of graph selected by the user in the radio items
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to update the graph depending on which continent we choose in the dropdown menu and which type of graph we choose in the radio items
def update_graph_continent_hpd(dropdown, radio_items, toggle): # Definition of the function to update the graph
    
    mask = (life_exp_continent_year.full_continent == dropdown) # The item selected in the dropdown corresponds to the value of continent that are in the column full_continent of life_exp_continent_year data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    if radio_items == 'line': # If the user selects line in radio item we return a line chart
        fig = px.line(life_exp_continent_year[mask], # We store the graph in fig variable
                      x='year', # x axis
                      y=['hepatitis_b','polio','diphtheria'], # y axis
                      labels={'year':'Years', 'value':'Immunization Coverage (%)'}, # x and y axis names
                      markers=True,  # markers to have points on the line
                                        template=template) # template selected
        return fig # We return the line plot 
    
    if radio_items == 'bar': # If the user selects bar in radio item we return a bar chart
        fig2 = px.bar(life_exp_continent_year[mask], # We store the graph in fig2 variable
                      x='year', # x axis
                      y=['hepatitis_b','polio','diphtheria'], # y axis
                      labels={'year':'Years', 'value':'Immunization Coverage (%)'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected 
        return fig2 # we return the bar chart
    



   
    

############################################ Tab 3
############### Sub tab 11 (fourth sub tab of tab 3) Worldwide Alcohol Consumption 


# Callback for sub tab 11 in tab 3 (fourth sub tab of tab 3) Worldwide Alcohol Consumption 
@app.callback(Output('subtab11', 'children'), # Output is the content of sub tab 11 in tab 3
              [Input('subtabs8', 'value'),# Input is the content of the different sub tabs that are in tab 3
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected   

# Function to display the graph in sub tab 11 tab 3 (alcohol consumption)
def display_graph_subtab11(value, toggle): # Definition of the function
    if value == 'Subtab11': # If we are in sub tab 11 in tab 3 we return the graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
            html.Div(
                html.P("Mean of the Worldwide Alcohol Consumption between 2000 and 2015 (liters of pure alcohol)", # graph title
                       style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
                ),
            dcc.Graph(id='Mean_alcohol_line_plot', # dcc.graph allows to create a graph
                      figure = px.line(mean_life_exp, # We store the graph in fig variable
                                       x='Year', # x axis
                                       y='Alcohol', # y axis
                                       labels = {'Year':'Years', 'Alcohol' : 'Alcohol Consumption (liters of pure alcohol)'}, # x and y axis names
                                       markers=True,  # markers to have points on the line
                                        template=template))]) # template selected


############################################ Tab 3
############### Sub tab 12 (fifth sub tab of tab 3) Mean of the Worldwide Alcohol Consumption by country between 2000 and 2015 (liters)

# Callback for Tab 3 and sub tab 12 Mean of the Worldwide Alcohol Consumption by country between 2000 and 2015 (liters)
@app.callback(Output('subtab12', 'children'), # Output is the content of the fifth sub tab in tab 3
              [Input('subtabs8', 'value'), # Input is the content of the different sub tabs that we choose in tab 3
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])   # Another input is the theme selected   

# Function to create a dropdown menu and radio items (different countries that we can choose to see their alcohol consumption and radio items to choose between bar chart or line chart)
def dropdown_countries_alcohol(value, toggle):  # Definition of the function
    if value == 'Subtab12': # If we are in sub tab 12 in tab 3 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the country where you want to see the mean alcohol consumption between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # style of text
                    dcc.Dropdown(id='worldwide_dropdown_alcohol_country', # Create dropdown (list with all countries to choose the one where we want to see the mean alcohol consumption between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp['Country'].unique()], # the dropdown list contains each unique countries that are stored in the column Country of mean_life_exp data frame
                    placeholder= 'Select a continent', # Placeholder, text that appears within the dropdown
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of radio item
                    
                    dcc.Graph(id="scatter-plot-alcohol-country"),  # dcc.graph allows to create a graph
                    html.P("Mean of the Worldwide Alcohol Consumption by country between 2000 and 2015 (liters of pure alcohol)", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
                    ]) 

 


# Callback for Tab 3 and sub tab 12 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-alcohol-country", "figure"), # Output is the scatter-plot-alcohol-country
    [Input("worldwide_dropdown_alcohol_country", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown_alcohol_country
    [Input('radio_items_type_graph', 'value'), # Another input is the type of graph that we choose in the radio items
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Create a function to update the graph depending on the country selected in the dropdown menu
def update_graph_countries_alcohol(dropdown, radio_items, toggle):  # Definition of the function
    
    mask = (life_exp.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    if radio_items == 'line': # If the user selects the radio item line chart we return a line chart
        fig = px.line(life_exp[mask], # We store the graph in fig variable
                      x='Year', # x axis
                      y='Alcohol', # y axis
                      labels = {'Year':'Years', 'Alcohol' : 'Alcohol Consumption (liters of pure alcohol)'}, # x and y axis names
                      markers=True, # markers to have points on the line
                                        template=template) # template selected
        return fig # We return the line plot 
    
    if radio_items == 'bar': # If the user selects bar in radio item we return a bar chart
        fig2 = px.bar(life_exp[mask], # We store the graph in fig2 variable
                      x='Year', # x axis
                      y='Alcohol', # y axis
                      labels = {'Year':'Years', 'Alcohol' : 'Alcohol Consumption (liters of pure alcohol)'}, # x and y axis names
                                        template=template)  # template selected  
        return fig2 # we return the bar chart
    


############################################ Tab 3
############### Sub tab 13 (sixth sub tab of tab 3) Worldwide Alcohol Consumption by continent
 

# Callback for Tab 3 and subtab 13 Worldwide Alcohol Consumption by continent 
@app.callback(Output('subtab13', 'children'), # Output is the content of the sixth sub tab in tab 3
              [Input('subtabs8', 'value'),  # Input is the content of the different sub tabs that we choose in tab 3
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected  

# Function to create a dropdown menu and radio items (different continents that we can choose to see their alcohol consumption and radio items to choose the type of graph bar plot or line plot)
def dropdown_countries(value, toggle):  # Definition of the function
    if value == 'Subtab13': # If we are in sub tab 13 in tab 3 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the continent where you want to see the mean of alcohol consumption between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_alcohol_continent', # Create dropdown (list with all continents to choose the one where we want to see the mean alcohol consumption between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': continents, 'value': continents} for continents in life_exp_continent_year['full_continent'].unique()], # the dropdown list contains each unique continents that are stored in the column full_continent of life_exp_continent_year data frame
                    placeholder= 'Select a continent', # Placeholder, text that appears within the dropdown
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of radio item
                    
                    dcc.Graph(id="scatter-plot-alcohol-continent"), # dcc.graph allows to create a graph
                    html.P("Mean of the Worldwide Alcohol Consumption by continent between 2000 and 2015 (liters of pure alcohol)", # graph title
                            style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}), # style of  graph title
                    ]) 
 
 
# Callback for Tab 3 and sub tab 13 to display the graph (by continents)
@app.callback(
    Output("scatter-plot-alcohol-continent", "figure"), # Output is the scatter-plot-alcohol-continent
    [Input("worldwide_dropdown_alcohol_continent", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown_alcohol_continent
    [Input("radio_items_type_graph","value"), # Another input is the type of graph selected in the radio items
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to update the graph depending on which continent we choose in the dropdown menu and which type of graph we choose in the radio items
def update_graph_continent_alcohol(dropdown, radio_items, toggle): # Definition of the function to update the graph
    mask = (life_exp_continent_year.full_continent == dropdown) # The item selected in the dropdown corresponds to the value of continent that are in the column full_continent of life_exp_continent_year data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    if radio_items == 'line':  # If the user selects line in radio item we return a line chart
        fig = px.line(life_exp_continent_year[mask],  # We store the graph in fig variable
                      x='year', # x axis
                      y='alcohol', # y axis
                      labels = {'year':'Years', 'alcohol' : 'Alcohol Consumption (liters of pure alcohol)'}, # x and y axis names
                      markers=True, # markers to have points on the line
                                        template=template) # template selected
        return fig # We return the line plot 
    
    if radio_items == 'bar': # If the user selects bar in radio item we return a bar chart
        fig2 = px.bar(life_exp_continent_year[mask], # We store the graph in fig2 variable
                      x='year', # x axis
                      y='alcohol', # y axis
                      labels = {'year': 'Years', 'alcohol' : 'Alcohol Consumption (liters of pure alcohol)'}, # x and y axus names
                                        template=template)  # template selected 
        return fig2 # we return the bar chart
        
    
       


   

############################################ Tab 3
############### Sub tab 14 (seventh sub tab of tab 3) Number of Worldwide Reported Cases having Measles between 2000 and 2015 (reported cases for 1000 habitants)

# Callback for sub tab 14 in tab 3 (seventh subtab of tab 3) Number of Worldwide Reported Cases having Measles between 2000 and 2015 (reported cases for 1000 habitants)
@app.callback(Output('subtab14', 'children'), # Output is the content of subtab 14 in tab 3
              [Input('subtabs8', 'value'),# Input is the content of the different sub tabs that are in tab 3
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected  


# Create function to display the graph in sub tab 14
def display_graph_subtab14(value, toggle): # Definition of the function
    if value == 'Subtab14': # If we are in sub tab 14 in tab 3 we return a graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
            html.Div(
                html.P("Number of Worldwide Reported Cases having Measles between 2000 and 2015 (reported cases for 1000 habitants)", # graph title
                       style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
                ),
            dcc.Graph(id='Measles_line_plot', # dcc.graph allows to create a graph
                      figure = px.line(mean_life_exp, # We store the graph in figure variable
                                       x='Year', # x axis
                                       y='Measles', # y axis
                                       labels = {'Year':'Years', 'Measles' : 'Number of reported cases having measles'}, # x and y axus names
                                       markers=True, # markers to have points on the line
                                        template=template))]) # template selected
    

  
   

############################################ Tab 3
############### Sub tab 15 (eigth sub tab of tab 3) Number of Worldwide Reported Cases having Measles by country 


# Callback for Tab 3 and subtab 15 Number of Worldwide Reported Cases having Measles by country 
@app.callback(Output('subtab15', 'children'), # Output is the content of the eigth sub tab in tab 3
              [Input('subtabs8', 'value'), # Input is the content of the different sub tabs that we choose in tab 3
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])   # Another input is the theme selected

# Function to create a dropdown menu and radio items (different countries that we can choose to see the number of reported cases having measles between 2000 and 2015 and radio item to choose the type of graph bar plot or line plot)
def dropdown_countries_measles(value, toggle):  # Definition of the function
    if value == 'Subtab15': # If we are in subtab 15 in tab 3 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([# html components
                    html.P("Choose the country where you want to see the number of reported cases having measles between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_measles_country', # Create dropdown (list with all countries to choose the one where we want to see the number of reported cases having measles between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp['Country'].unique()], # the dropdown list contains each unique countries that are stored in the column Country of life_exp data frame
                    placeholder= 'Select a country', # Placeholder, text that appears within the dropdown
                    
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # style of placeholder
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of tadio items
                    dcc.Graph(id="scatter-plot-measles-country"), # dcc.graph allows to create a graph
                     html.P("Mean life expectancy worldwide by country between 2000 and 2015", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}),# Style of the graph title
                    ]) 




# Callback for Tab 3 and sub tab 15 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-measles-country", "figure"), # Output is the scatter-plot-measles-country
    [Input("worldwide_dropdown_measles_country", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown_measles_country
    [Input('radio_items_type_graph', 'value'),
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the type of graph selected in the radio items

# Create function to update graph depending on the country selected in the dropdown menu and depending on which type of graph is selected in radio items
def update_graph_countries_measles(dropdown, radio_items, toggle): # Definition of the function to update the graph
    mask = (life_exp.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp data frame
    template = template_theme1 if toggle else template_theme2
    
    if radio_items == 'line': # If the user selects the line chart in radio items we return a line chart
        fig = px.line(life_exp[mask], # We store the graph in fig variable
                      x='Year', # x axis
                      y='Measles', # y axis
                      labels = {'Year':'Years', 'Measles' : 'Number of reported cases having measles'}, # x and y axis names
                      markers=True, # markers to have points on the line
                                        template=template) # template selected
        return fig # We return the line plot 
    
    if radio_items == 'bar':  # If the user selects bar in radio item we return a bar chart
        fig2 = px.bar(life_exp[mask], # We store the graph in fig2 variable
                      x='Year', # x axis
                      y='Measles', # y axis
                      labels = {'Year':'Years', 'Measles' : 'Number of reported cases having measles'}, # x and y axis names
                                        template=template) # template selected 
        return fig2 # we return the bar chart
    



   
############################################ Tab 3
############### Sub tab 16 (nineth sub tab of tab 3) Number of Worldwide Reported Cases having Measles by continent
 

# Callback for Tab 3 and subtab 16 Number of Worldwide Reported Cases having Measles by continent 
@app.callback(Output('subtab16', 'children'), # Output is the content of the nineth sub tab in tab 3
              [Input('subtabs8', 'value'),  # Input is the content of the different sub tabs that we choose in tab 3
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected   


# Function to create a dropdown menu and radio items (different continents that we can choose to see the number of reported cases having measles and radio items to choose bar plot or scatter plot)
def dropdown_continent_measles(value, toggle):  # Definition of the function
    if value == 'Subtab16': # If we are in subtab 16 in tab 3 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([# html components
                    html.P("Choose the continent where you want to see the number of reported cases having measles between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_measles_continent', # Create dropdown (list with all continents to choose the one where we want to see the number of reported cases having measles between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': continents, 'value': continents} for continents in life_exp_continent_year['full_continent'].unique()], # the dropdown list contains each unique continents that are stored in the column full_continent of life_exp_continent_year data frame
                    placeholder= 'Select a continent', # Placeholder, text that appears within the dropdown
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of radio item
                    dcc.Graph(id="scatter-plot-measles-continent"), # dcc.graph allows to create a graph
                    html.P("Number of Worldwide Reported Cases having Measles By Continent between 2000 and 2015 (reported cases for 1000 habitants)", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'})# Style of graph title
                    ]) 

    
# Callback for Tab 3 and sub tab 16 to display the graph (by continents)
@app.callback(
    Output("scatter-plot-measles-continent", "figure"), # Output is the scatter-plot-measles-continent
    [Input("worldwide_dropdown_measles_continent", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown_measles_continent
    [Input("radio_items_type_graph","value"), # Another input is the type of graph selected in the radio items
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected  

# Function to update the graph depending on which continent we choose in the dropdown menu and which type of graph we choose in the radio items
def update_graph_continent_measles(dropdown, radio_items, toggle): # Definition of the function to update the graph
    
    mask = (life_exp_continent_year.full_continent == dropdown) # The item selected in the dropdown corresponds to the value of continent that are in the column full_continent of life_exp_continent_year data frame
    template = template_theme1 if toggle else template_theme2   # We define the theme selected
    if radio_items == 'line': # If the user selects line in radio item we return a line chart
        fig = px.line(life_exp_continent_year[mask], # We store the graph in fig variable
                      x='year', # x axis
                      y='measles', # y axis
                      labels = {'year':'Years', 'measles' : 'Number of reported cases having measles'}, # x and y axis names
                      markers=True, # markers to have points on the line
                                        template=template) # template selected
        return fig # We return the line plot 
    
    if radio_items == 'bar': # If the user selects bar in radio item we return a bar chart
        fig2 = px.bar(life_exp_continent_year[mask],  # We store the graph in fig2 variable
                      x='year', # x axis
                      y='measles', # y axis
                      labels = {'year':'Years', 'measles' : 'Number of reported cases having measles'}, # x and y axis names
                                        template=template) # template selected  
        return fig2 # we return the bar chart
    
    
    
    
        
############################################ Tab 4
############### Sub tab 17 (first sub tab of tab 4) Life Expectancy Worldwide By Developed Countries 


# Callback for Tab 4 and subtab 17  Mean Life Expectancy Worldwide for developed countries, developing countries and both at the same time (between 2000-2015)
@app.callback(Output('subtab17', 'children'), # Output (what we see) is the content of the first subtab in tab 4
              [Input('subtabs17', 'value'),  # Input (what we give) is the content of the different subtabs that we choose in tab 4
               Input(ThemeSwitchAIO.ids.switch("theme"),"value") ]) # Another input is the theme selected  

# Function to display a graph in the first subtab in tab 4
def display_graph_subtab17(value, toggle):
    
    if value == 'Subtab17': # If we are in subtab17 in tab 4 we return a graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
            html.Div(
            html.P("Mean Life Expectancy Worldwide for developed countries, developing countries and both at the same time between 2000-2015", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
            ),
            
            dcc.Graph(id="Mean_life_exp", # dcc.graph allows to create a graph
                      figure = px.line(mean_life_exp, # We store the graph in figure variable
                                       x="Year", # x axis
                                       y=["Life_exp_developed", "Life_exp", "Life_exp_developing"], # y axis 
                                       labels = {'x':'Years', 'value' : 'Life Expectancy'}, # x and y axis names
                                       markers=True, # markers to have points on the line
                                       template=template))]) # template selected
 

############################################ Tab 4
############### Sub tab 18 (second sub tab of tab 4) Life Expectancy Worldwide By Developed Countries
 
# Callback for Tab 4 and subtab 18
@app.callback(Output('subtab18', 'children'), # Output is the content of the second sub tab in tab 4
              [Input('subtabs17', 'value'),  # Input is the content of the different sub tabs that we choose in tab 4
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected  

# Function to create a dropdown menu and radio items (different countries that we can choose to see their life expectancy and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab18': # If we are in subtab18 in tab 4 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the developed country where you want to see the mean life expectancy between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_developed', # Create dropdown (list with all countries to choose the one where we want to see the mean life expectancy between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developed['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp_developed data frame
                    placeholder= 'Select the developed country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see",  # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of radio item
                    dcc.Graph(id="scatter-plot-developed"), # dcc.graph allows to create a graph
                    html.P("Mean life expectancy Worldwide  By developed countries between 2000 and 2015", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # Style of graph title
                    ]) 
 
# Callback to update graph depending on dropdown and radio items selection
@app.callback(
    Output("scatter-plot-developed", "figure"), # Output is the scatter-plot-developed
    [Input("worldwide_dropdown_developed", "value")], # Input is the data from the list in the dropdown 
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to update the graph
def update_graph_countries_subtab18(dropdown, radio_items, toggle): # Create a function to update the graph
    mean_life_exp_developed["level"] = [0 for i in range(len(mean_life_exp_developed))]
    mask = (life_exp_developed.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp_developed data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    
    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(life_exp_developed[mask], x = "Year", # x axis
                      y = ["Life_exp", "Life_exp_developed", "Life_exp_developing"], # y axis
                      labels = {'x':'Years', 'value' : 'Life Expectancy'}, #x and y axis names
                      markers=True, # markers to have points on the line
                      template=template # template selected
                      )
        return fig # We return graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(life_exp_developed[mask], x = "Year", # x axis
                      y = ["Life_exp", "Life_exp_developed", "Life_exp_developing"], # y axis
                      labels = {'x':'Years', 'value' : 'Life Expectancy'}, # x and y axis names
                      barmode='group',  # to have multiple bars (one bar for each variable)
                      template=template)  # template selected 
        

        return fig2 # We return graph called fig2

    






############################################ Tab 4
############### Sub tab 19 (third sub tab of tab 4) Life Expectancy Worldwide By Developing Countries
 

# Callback for Tab 4 and subtab 19
@app.callback(Output('subtab19', 'children'), # Output is the content of the third sub tab in tab 4
              [Input('subtabs17', 'value'), # Input is the content of the different sub tabs that we choose in tab 4
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected

# Function to create a dropdown menu and radio items (different countries that we can choose to see their life expectancy and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab19': # If we are in subtab19 in tab 4 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the developing country where you want to see the mean life expectancy between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_developing', # Create dropdown (list with all countries to choose the one where we want to see the mean life expectancy between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developing['Country'].unique()], # the dropdown list contains each unique countries that are stored in the column Country of life_exp_developing data frame
                    placeholder= 'Select the developing country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder 
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}) # style of text
                    ,
                    dcc.Graph(id="scatter-plot-developing"),  # dcc.graph allows to create a graph
                    html.P("Mean life expectancy Worldwide  By developing countries between 2000 and 2015", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'})# Style of graph title
                    ]) 
 
    
# Callback to update the graph depending on dropdown and radio items selection
@app.callback(
    Output("scatter-plot-developing", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_developing", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected  

# Function to update the graph depending on dropdown and radio items selection
def update_graph_countries_subtab19(dropdown, radio_items, toggle): # Create a function to update the graph
    
    mask = (life_exp_developing.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp_developing data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    temp_life_exp_developing = life_exp_developing[mask] # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developing["Life_exp_developed"] = list(mean_life_exp_developed.Life_exp) # create column Life_exp_developed
    temp_life_exp_developing["Life_exp_developing"] = list(mean_life_exp_developing.Life_exp) # create column Life_exp_developing

    if radio_items =='line': # If the user chooses the radio item line we return a line plot
    
        fig = px.line(temp_life_exp_developing, # dataframe used
                      x = "Year", # x axis
                      y= ["Life_exp", "Life_exp_developed", "Life_exp_developing"], # y axis
                      labels = {'x':'Years', 'value' : 'Life Expectancy'}, # x and y axis names
                      markers=True, # markers to have points on the line
                      template=template) # template selected
        
        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
    
        fig2 = px.bar(temp_life_exp_developing, # dataframe used
                      x = "Year", # x axis
                      y= ["Life_exp", 'Life_exp_developed', 'Life_exp_developing'], # y axis
                      labels = {'x':'Years', 'value' : 'Life Expectancy'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected 
        
        return fig2 # We return the graph called fig2
 
    


############################################ Tab 5
############### Sub tab 20 (first sub tab of tab 5) Mean Adult Death Worldwide 
 
# Callback for Tab 5 and subtab 20 
@app.callback(Output('subtab20', 'children'), # Output (what we see) is the content of the first subtab in tab 5
              [Input('subtabs20', 'value'), # Input (what we give) is the content of the different subtabs that we choose in tab 5
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to display a graph in the first subtab in tab 5
def display_graph_subtab20(value, toggle):
    
    if value == 'Subtab20': # If we are in subtab20 in tab 5 we return a graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        
        return html.Div([ # html components
            html.Div(
            html.P("Mean Adult Death Worldwide for Developed Countries, Developing Countries and both at the same time (between 2000-2015)", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
            ),
            dcc.Graph(id="Mean_death", # dcc.graph allows to make a Graph
                                   
                                   figure = px.line (mean_life_exp, # creare graph in figure variable
                                                     x = 'Year', # x axis
                                                     y= ['Adult_Mortality','Adult_Mortality_developed_countries', # y axis
                                                         'Adult_Mortality_developing_countries'],
                                                     labels = {'x':'Years', 'value' : 'Adult Mortality'}, # x and y axis names
                                                     markers=True, # markers to have points on the line
                                                     template = template))]) # template selected
        
        
############################################ Tab 5
############### Sub tab 21 (second sub tab of tab 5) Mean Adult Death Worldwide By Developed Countries (between 2000-2015)
 
# Callback for Tab 5 and subtab 21
@app.callback(Output('subtab21', 'children'), # Output is the content of the second sub tab in tab 5
              [Input('subtabs20', 'value'), # Input is the content of the different sub tabs that we choose in tab 5
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected   

# Function to create a dropdown menu and radio items (different countries that we can choose to see their adult mortality and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab21': # If we are in subtab21 in tab 5 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the developed country where you want to see the mean adult mortality between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_adult', # Create dropdown (list with all countries to choose the one where we want to see the mean adult mortality between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developed['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp_developed data frame
                    placeholder= 'Select the developed country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}),
                    
                    dcc.Graph(id="scatter-plot-adult"),  # dcc.graph allows to create a graph
                    html.P("Mean Adult Death Worldwide By Developed Countries (between 2000-2015)", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'})# Style of graph title
                    ]) 
    

# Callback for Tab 5 and sub tab 21 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-adult", "figure"), # Output is the scatter-plot-adult
    [Input("worldwide_dropdown_adult", "value")], # Input is the data from the list in the dropdown 
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Creation of a function to update the graph depending on dropdown and radio items selection
def update_graph_countries_subtab21(dropdown, radio_items, toggle): # Create a function to update the graph
    
    mask = (life_exp_developed.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp_developed data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    temp_life_exp_developed = life_exp_developed[mask] # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developed["Adult_Mortality_developed"] = list(mean_life_exp_developed.Adult_Mortality) # create column Adult_Mortality_developed
    temp_life_exp_developed["Adult_Mortality_developing"] = list(mean_life_exp_developing.Adult_Mortality) # create column Adult_Mortality_developing

    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developed, x = "Year", # x axis
                y= ["Adult_Mortality", "Adult_Mortality_developed", # y axis
                    "Adult_Mortality_developing"],
                labels = {'x':'Years', 'value' : 'Adult Mortality'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # template selected
                

        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
    
        fig2 = px.bar(temp_life_exp_developed, x = "Year", # x axis
                      y= ["Adult_Mortality", "Adult_Mortality_developed", # y axis
                          "Adult_Mortality_developing"],
                      labels = {'x':'Years', 'value' : 'Adult Mortality'},# x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected 
        
        return fig2 # We return the graph called fig2



############################################ Tab 5
############### Sub tab 22 (third sub tab of tab 5) Mean Adult Death Worldwide By Developing Countries (between 2000-2015)
# Callback for Tab 5 and subtab 22
@app.callback(Output('subtab22', 'children'), # Output is the content of the second sub tab in tab 5
              [Input('subtabs20', 'value'), # Input is the content of the different sub tabs that we choose in tab 5
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

def dropdown_countries(value, toggle):  
    if value == 'Subtab22': # If we are in subtab21 in tab 5 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the developing country where you want to see the mean adult mortality between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_adult_developing', # Create dropdown (list with all countries to choose the one where we want to see the mean adult mortality between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developing['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp_developed data frame
                    placeholder= 'Select the developing country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}),
                    dcc.Graph(id="scatter-plot-adult-developing"), # dcc.graph allows to create a graph
                    html.P("Mean Adult Death Worldwide By developing Countries (between 2000-2015)", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
                    ]) 



# Callback for Tab 5 and sub tab 22 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-adult-developing", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_adult_developing", "value")], # Input is the data from the list in the dropdown 
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected 

# Creation of a function to update the graph
def update_graph_countries_subtab22(dropdown, radio_items, toggle): # Create a function to update the graph
    
    mask = (life_exp_developing.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp_developing data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    
    temp_life_exp_developing = life_exp_developing[mask] # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developing["Adult_Mortality_developed"] = list(mean_life_exp_developed.Adult_Mortality) # create column Life_exp_developed
    temp_life_exp_developing["Adult_Mortality_developing"] = list(mean_life_exp_developing.Adult_Mortality) # create column Life_exp_developing


    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developing, x = "Year", # x axis
                y= ["Adult_Mortality", "Adult_Mortality_developed", # y axis
                    "Adult_Mortality_developing"],
                labels = {'x':'Years', 'value' : 'Adult Mortality'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template)  # template selected
        
        
        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developing, x = "Year", # x axis
                      y= ["Adult_Mortality", "Adult_Mortality_developed", # y axis
                    "Adult_Mortality_developing"],
                      labels = {'x':'Years', 'value' : 'Adult Mortality'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected 
        return fig2 # We return the graph called fig2






 ############################################ Tab 5
############### Sub tab 23 (fourth sub tab of tab 5) Mean Infant Death Worldwide for Developed Countries, Developing Countries and both at the same time (between 2000-2015)

# Callback for Tab 5 and subtab 23
@app.callback(Output('subtab23', 'children'), # Output (what we see) is the content of the fourth subtab in tab 5
              [Input('subtabs20', 'value'), # Input (what we give) is the content of the different subtabs that we choose in tab 5
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to display a graph in the first subtab in tab 1
def display_graph_subtab23(value, toggle):
    
    if value == 'Subtab23': # If we are in subtab23 in tab 5 we return a graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected
        
        return html.Div([ #  html components
            html.Div(
            html.P("Mean Infant Death Worldwide for Developed Countries, Developing Countries and both at the same time (between 2000-2015)", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
            ),dcc.Graph(id="Mean_infant", # dcc.graph allows to make a Graph
                                   
                                   figure =  px.line(mean_life_exp, x= "Year", # x axis
                                                     y= ["Infant_Deaths", 'Infant_Mortality_developed_countries', # y axis
                                                         'Infant_Mortality_developing_countries'],
                                                     labels = {'x':'Years', 'value' : 'Infant Mortality'}, # x and y axis names
                                                     markers=True, # markers to have points on the line
                                                     
                                                     template=template) # template selected
                                   )]) 







############################################ Tab 5
############### Sub tab 24 (fifth sub tab of tab 5) Mean Infant Death Worldwide By Developed Countries (between 2000-2015)

# Callback for Tab 5 and subtab 24
@app.callback(Output('subtab24', 'children'), # Output is the content of the fifth sub tab in tab 5
              [Input('subtabs20', 'value'), # Input is the content of the different sub tabs that we choose in tab 5
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected   

# Function to create a dropdown menu and radio items (different countries that we can choose to see their infzant mortality and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab24': # If we are in subtab24 in tab 5 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the developed country where you want to see the mean infant mortality between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_infant', # Create dropdown (list with all countries to choose the one where we want to see the mean infant mortality between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developed['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp_developed data frame
                    placeholder= 'Select the developed country',
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # Placeholder, text that appears within the dropdown 
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                   
                    dcc.Graph(id="scatter-plot-infant"), # dcc.graph allows to create a graph
                     html.P("Mean Infant Death Worldwide By Developed Countries (between 2000-2015)", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'})# Style of graph title
                    ]) 
   
# Callback for Tab 5 and sub tab 24 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-infant", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_infant", "value")], # Input is the data from the list in the dropdown
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected   

# Create function to update the graph depending on dropdown and radio items selection
def update_graph_countries_subtab24(dropdown, radio_items, toggle): # Create a function to update the graph
    
    mask = (life_exp_developed.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp_developed data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    temp_life_exp_developed = life_exp_developed[mask] # dataframe that contains the data for the specific country
    temp_life_exp_developed["Infant_Deaths_developed"] = list(mean_life_exp_developed.Infant_Deaths) # create column Infant_Deaths_developed
    temp_life_exp_developed["Infant_Deaths_developing"] = list(mean_life_exp_developing.Infant_Deaths) # create column Infant_Deaths_developing

    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developed, # create graph stored in fig variable
                      x="Year", # x axis
                y= ["Infant_Deaths", 'Infant_Deaths_developed', # y axis
                    'Infant_Deaths_developing'],
                labels = {'x':'Years', 'value' : 'Infant Mortality'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # # template selected
                

        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developed,
                      x="Year", # x axis
                y= ["Infant_Deaths", 'Infant_Deaths_developed', # y axis
                    'Infant_Deaths_developing'],
                      labels = {'x':'Years', 'value' : 'Infant Mortality'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected
        
        return fig2 # We return the graph called fig2




############################################ Tab 5
############### Sub tab 25 (sixth sub tab of tab 5) Mean Infant Death Worldwide By Developing Countries (between 2000-2015)

# Callback for Tab 5 and subtab 25 
@app.callback(Output('subtab25', 'children'), # Output is the content of the sixth sub tab in tab 5
              [Input('subtabs20', 'value'), # Input is the content of the different sub tabs that we choose in tab 5
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected   

# Function to create a dropdown menu and radio items (different countries that we can choose to see their infant mortality and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab25': # If we are in subtab2 in tab 1 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected


        return html.Div([ # html components
                    html.P("Choose the developing country where you want to see the mean infant mortality between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_infant_developing', # Create dropdown (list with all countries to choose the one where we want to see the mean infant mortality between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developing['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp_developing data frame
                    placeholder= 'Select the developing country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see",  # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                   
                    dcc.Graph(id="scatter-plot-infant-developing"), # dcc.graph allows to create a graph
                     html.P("Mean Infant Death Worldwide By Developing Countries (between 2000-2015)", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'})# Style of graph title
                    ]) 
   
# Callback for Tab 5 and sub tab 25 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-infant-developing", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_infant_developing", "value")], # Input is the data from the list in the dropdown 
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected

# Create function to update the graph
def update_graph_countries_subtab25(dropdown, radio_items, toggle): # Create a function to update the graph
    
    mask = (life_exp_developing.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    temp_life_exp_developing = life_exp_developing[mask] # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developing["Infant_Deaths_developed"] = list(mean_life_exp_developed.Infant_Deaths) # create column Infant_Deaths_developed
    temp_life_exp_developing["Infant_Deaths_developing"] = list(mean_life_exp_developing.Infant_Deaths) # create column Infant_Deaths_developing


    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developing, x= "Year", # x axis
                y= ["Infant_Deaths", 'Infant_Deaths_developed', # y axis
                    'Infant_Deaths_developing'],
                labels = {'x':'Years', 'value' : 'Infant Mortality'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # template selected


        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developing, x= "Year", # x axis
                y= ["Infant_Deaths", 'Infant_Deaths_developed', # y axis
                    'Infant_Deaths_developing'],
                      labels = {'x':'Years', 'value' : 'Infant Mortality'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected 

        
        return fig2 # We return the graph called fig2
    






############################################ Tab 6
############### Sub tab 26 (first sub tab of tab 6) Mean of the Worldwide Immunization Coverage among 1-year-olds for Hepatitis B in Developed Countries, Developing Countries and both at the same time (between 2000-2015 in %)

# Callback for Tab 6 and subtab 26 
@app.callback(Output('subtab26', 'children'), # Output (what we see) is the content of the first subtab in tab 6
              [Input('subtabs26', 'value'), # Input (what we give) is the content of the different subtabs that we choose in tab 6
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to display a graph in the first subtab in tab 6
def display_graph_subtab26(value, toggle):
    
    if value == 'Subtab26': # If we are in subtab26 in tab 6 we return a graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        
        return html.Div([ # h tml components
            html.Div(
            html.P("Mean of the Worldwide Immunization Coverage among 1-year-olds for Hepatitis B in Developed Countries, Developing Countries and both at the same time (between 2000-2015 in %)", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
            ),dcc.Graph(id="Mean_hepatitis", # dcc.graph allows to make a Graph
                                   
                                   figure = px.line (mean_life_exp, # create graph stored in figure variable
                                                     x = 'Year', # x axis
                                                     y= ['Hepatitis_B','Hepatitis_B_developed_countries', # y axis
                                                         'Hepatitis_B_developing_countries'],
                                                     labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                                                     markers=True, # markers to have points on the line
                                                     template=template))]) # template selected
 

    
############################################ Tab 6
############### Sub tab 27 (second sub tab of tab 6) Mean of the Worldwide Immunization Coverage among 1-year-olds By Developed Countries for Hepatitis B (between 2000-2015 in %)

# Callback for Tab 6 and subtab 27
@app.callback(Output('subtab27', 'children'), # Output is the content of the second sub tab in tab 6
              [Input('subtabs26', 'value'), # Input is the content of the different sub tabs that we choose in tab 6
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected   

# Function to create a dropdown menu and radio items (different countries that we can choose to see their immunisation coverage for hepatitis b and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab27': # If we are in subtab2 in tab 1 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # Create graph called scatter-plot
                    html.P("Choose the developed country where you want to see the Worldwide Immunization Coverage among 1-year-olds for Hepatitis B between 2000 and 2015",  # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_hepatitis', # Create dropdown (list with all countries to choose the one where we want to see the immunization coverage for hepatitis b between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developed['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp_developed data frame
                    placeholder= 'Select the developed country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                    
                    dcc.Graph(id="scatter-plot-hepatitis"),  # dcc.graph allows to create a graph
                    html.P("Mean of the Worldwide Immunization Coverage among 1-year-olds By Developed Countries for Hepatitis B (between 2000-2015 in %)", # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'})# Style of graph title
                    ]) 
   
# Callback for Tab 6 and sub tab 27 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-hepatitis", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_hepatitis", "value")], # Input is the data from the list in the dropdown 
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected

# Create a function to update the graph
def update_graph_countries_subtab27(dropdown, radio_items, toggle): 
    
    mask = (life_exp_developed.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp_developed data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    temp_life_exp_developed = life_exp_developed[mask] # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developed["Hepatitis_B_developed"] = list(mean_life_exp_developed.Hepatitis_B) # create column Hepatitis_B_developed
    temp_life_exp_developed["Hepatitis_B_developing"] = list(mean_life_exp_developing.Hepatitis_B) # create column Hepatitis_B_developing
    
    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developed, x = 'Year', # x axis
                                                     y= ['Hepatitis_B','Hepatitis_B_developed', # y axis
                                                         'Hepatitis_B_developing'],
                labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # template selected
                

        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developed, x = 'Year', # x axis
                                                     y= ['Hepatitis_B','Hepatitis_B_developed', # y axis
                                                         'Hepatitis_B_developing'],
                      labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected 
        
        return fig2 # We return the graph called fig2




############################################ Tab 6
############### Sub tab 28 (third sub tab of tab 6) Mean of the Worldwide Immunization Coverage among 1-year-olds for Polio in Developed countries, Developing Countries and both at the same time (between 2000-2015 in %)

# Callback for Tab 6 and subtab 28 
@app.callback(Output('subtab28', 'children'), # Output (what we see) is the content of the third subtab in tab 6
              [Input('subtabs26', 'value'), # Input (what we give) is the content of the different subtabs that we choose in tab 6
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to display a graph in the third subtab in tab 6
def display_graph_subtab28(value, toggle):
    
    if value == 'Subtab28': # If we are in subtab28 in tab 6 we return a graph
        template = template_theme1 if toggle else template_theme2  # We define the theme selected

        
        return html.Div([ # html components
            html.Div(
            html.P("Mean of the Worldwide Immunization Coverage among 1-year-olds for Polio in Developed countries, Developing Countries and both at the same time (between 2000-2015 in %)", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of  graph title
            ),dcc.Graph(id="Mean_Polio", # dcc.graph allows to make a Graph
                                   
                                   figure = px.line (mean_life_exp, # create graph stored in figure variable
                                                     x ='Year',  # x axis
                                                     y= ['Polio', # y axis
                                                         'Polio_developed_countries','Polio_developing_countries'],
                                                     labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                                                     markers=True, # markers to have points on the line
                                                     template=template))]) # template selected



############################################ Tab 6
############### Sub tab 29 (fourth sub tab of tab 6) Mean of the Worldwide Immunization Coverage among 1-year-olds By Developed Countries for Polio (between 2000-2015 in %)
# Callback for Tab 6 and subtab 29
@app.callback(Output('subtab29', 'children'), # Output is the content of the fourth sub tab in tab 6
              [Input('subtabs26', 'value'), # Input is the content of the different sub tabs that we choose in tab 6
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected    

# Function to create a dropdown menu and radio items (different countries that we can choose to see their immunization coverage for Polio and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab29': # If we are in subtab29 in tab 6 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # Create graph called scatter-plot
                    html.P("Choose the developed country where you want to see the Worldwide Immunization Coverage among 1-year-olds for Polio between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_Polio', # Create dropdown (list with all countries to choose the one where we want to see the immunization coverage for Polio between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developed['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp_developed data frame
                    placeholder= 'Select the developed country', # Placeholder, text that appears within the dropdown
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                    dcc.Graph(id="scatter-plot-Polio"), # dcc.graph allows to create a graph
                                        html.P(' Mean of the Worldwide Immunization Coverage among 1-year-olds By Developed Countries for Polio (between 2000-2015 in %)',  # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # Style of graph title
                    ]) 
   
# Callback for Tab 6 and sub tab 29 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-Polio", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_Polio", "value")], # Input is the data from the list in the dropdown 
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  


def update_graph_countries_subtab29(dropdown, radio_items, toggle): # Create a function to update the graph
    
    mask = (life_exp_developed.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp_developed data frame
    template = template_theme1 if toggle else template_theme2  # We define the theme selected
    temp_life_exp_developed = life_exp_developed[mask] # dataframe that contains the data for the specific country selected
    temp_life_exp_developed["Polio_developed"] = list(mean_life_exp_developed.Polio) # create column Polio_developed
    temp_life_exp_developed["Polio_developing"] = list(mean_life_exp_developing.Polio) # create column Polio_developing

    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developed, x = 'Year', # x axis
                                                     y= ['Polio','Polio_developed',  # y axis
                                                         'Polio_developing'],
                labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # template selected
                

        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developed, x = 'Year', # x axis
                                                     y= ['Polio','Polio_developed',# y axis
                                                         'Polio_developing'],
                      labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected 
        
        return fig2 # We return the graph called fig2
    


############################################ Tab 6
############### Sub tab 30 (fifth sub tab of tab 6) Mean of the Worldwide Immunization Coverage among 1-year-olds for Diphteria in Developed countries, Developing Countries and both at the same time (between 2000-2015 in %)

# Callback for Tab 6 and subtab 30
@app.callback(Output('subtab30', 'children'), # Output (what we see) is the content of the fifth subtab in tab 6
              [Input('subtabs26', 'value'), # Input (what we give) is the content of the different subtabs that we choose in tab 6
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to display a graph in the fifth subtab in tab 6
def display_graph_subtab30(value, toggle):
    
    if value == 'Subtab30': # If we are in subtab 30 in tab 6 we return a graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        
        return html.Div([ #  html components
            html.Div(
            html.P("Mean of the Worldwide Immunization Coverage among 1-year-olds for Diphteria in Developed countries, Developing Countries and both at the same time (between 2000-2015 in %)", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
            ),dcc.Graph(id="Mean_diphteria", # dcc.graph allows to make a Graph
                                   
                                   figure = px.line (mean_life_exp, x ='Year',  # x axis
                                                     y= ['Diphteria','Diphteria_developed_countries', # y axis
                                                         'Diphteria_developing_countries'],
                                                     labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                                                     markers=True,  # markers to have points on the line
                                                     template=template))]) # template selected



############################################ Tab 6
############### Sub tab 31 (sixth sub tab of tab 6) Mean of the Worldwide Immunization Coverage among 1-year-olds By Developed Countries for Diphteria (between 2000-2015 in %)

# Callback for Tab 6 and subtab 31 
@app.callback(Output('subtab31', 'children'), # Output is the content of the sixth sub tab in tab 6
              [Input('subtabs26', 'value'), # Input is the content of the different sub tabs that we choose in tab 6
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])   # Another input is the theme selected

# Function to create a dropdown menu and radio items 
def dropdown_countries(value, toggle):  
    if value == 'Subtab31': # If we are in subtab31 in tab 6 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the developed country where you want to see the Worldwide Immunization Coverage among 1-year-olds for Diphteria between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # style of text
                    dcc.Dropdown(id='worldwide_dropdown_diphteria', # Create dropdown 
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developed['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp_developed data frame
                    placeholder= 'Select the developed country',  # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                    
                    dcc.Graph(id="scatter-plot-diphteria"),  # dcc.graph allows to create a graph
                    html.P('Mean of the Worldwide Immunization Coverage among 1-year-olds By Developed Countries for Diphteria (between 2000-2015 in %)', # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'})# Style of graph title
                    ]) 
   
# Callback for Tab 6 and sub tab 31 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-diphteria", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_diphteria", "value")], # Input is the data from the list in the dropdown
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  


def update_graph_countries_subtab31(dropdown, radio_items, toggle): # Create a function to update the graph
    
    mask = (life_exp_developed.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp_developed data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    temp_life_exp_developed = life_exp_developed[mask] # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developed["Diphteria_developed"] = list(mean_life_exp_developed.Diphteria) # create column Diphteria_developed
    temp_life_exp_developed["Diphteria_developing"] = list(mean_life_exp_developing.Diphteria) # create column Diphteria_developing

    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developed, x = 'Year', # x axis
                                                     y= ['Diphteria','Diphteria_developed', # y axis
                                                         'Diphteria_developing'],
                labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # template selected
                

        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developed, x = 'Year', # x axis
                                                     y= ['Diphteria','Diphteria_developed', # y axis
                                                         'Diphteria_developing'],
                      labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected
        
        return fig2 # We return the graph called fig2




############################################ Tab 6
############### Sub tab 32 (seventh sub tab of tab 6) Mean of the Worldwide Alcohol Consumption in Developed Countries, Developing Countries and both at the same time between 2000 and 2015 (liters of pure alcohol)

# Callback for Tab 6 and subtab 32 
@app.callback(Output('subtab32', 'children'), # Output (what we see) is the content of the seventh subtab in tab 6
              [Input('subtabs26', 'value'), # Input (what we give) is the content of the different subtabs that we choose in tab 6
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to display a graph in the seventh subtab in tab 6
def display_graph_subtab32(value, toggle):
    
    if value == 'Subtab32': # If we are in subtab 32 in tab 6 we return a graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        
        return html.Div([ # html components
            html.Div(
            html.P("Mean of the Worldwide Alcohol Consumption in Developed Countries, Developing Countries and both at the same time between 2000 and 2015 (liters of pure alcohol)", # Graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
            ),dcc.Graph(id="Mean_alcohol", # dcc.graph allows to make a Graph
                                   
                                   figure = px.line (mean_life_exp, x = 'Year', # store graph in figure variable
                                                     y= ['Alcohol', 'Alcohol_developed_countries', # y axis
                                                         'Alcohol_developing_countries'],
                                                     labels = {'x':'Years', 'value' : 'Alcohol Consumption (liters of pure alcohol)'}, # x and y axis names
                                                     markers=True, # markers to have points on the line
                                                     template=template))]) # template selected


############################################ Tab 6
############### Sub tab 33 (eigth sub tab of tab 6) Mean of the Worldwide Alcohol Consumption By Developed Countries between 2000 and 2015 (liters of pure alcohol)

# Callback for Tab 6 and subtab 33 
@app.callback(Output('subtab33', 'children'), # Output is the content of the eigth sub tab in tab 6
              [Input('subtabs26', 'value'),  # Input is the content of the different sub tabs that we choose in tab 6
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])   # Another input is the theme selected  

# Function to create a dropdown menu and radio items 
def dropdown_countries(value, toggle):  
    if value == 'Subtab33': # If we are in subtab33 in tab 6 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the developed country where you want to see the Worldwide Alcohol consumption between 2000 and 2015 (liters of pure alcohol)", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text 
                    dcc.Dropdown(id='worldwide_dropdown_alcohol', # Create dropdown (list with all countries to choose the one where we want to see the alcohol consumption between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developed['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp_developed data frame
                    placeholder= 'Select the developed country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),  # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                    dcc.Graph(id="scatter-plot-alcohol"), # dcc.graph allows to create a graph
                    html.P('Mean of the Worldwide Alcohol Consumption By Developed Countries between 2000 and 2015 (liters of pure alcohol)', # graph title
                           style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}),# Style of graph title
                    ]) 
   



# Callback for Tab 6 and sub tab 33 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-alcohol", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_alcohol", "value")], # Input is the data from the list in the dropdown 
    [Input('radio_items_type_graph', 'value'),  # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected

# Create a function to update the graph
def update_graph_countries_subtab33(dropdown, radio_items, toggle): 
    template = template_theme1 if toggle else template_theme2   # We define the theme selected
    mask = (life_exp_developed.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp_developed data frame
    temp_life_exp_developed = life_exp_developed[mask] # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developed["Alcohol_developed"] = list(mean_life_exp_developed.Alcohol) # create column Alcohol_developed
    temp_life_exp_developed["Alcohol_developing"] = list(mean_life_exp_developing.Alcohol) # create column Alcohol_developing

    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developed,x = "Year", # store graph in fig variable
                y= ["Alcohol", 'Alcohol_developed', 'Alcohol_developing'], # y axis
                labels = {'x':'Years', 'value' : 'Alcohol Consumption (liters of pure alcohol)'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # template selected
                

        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developed,x = "Year", # store graph in fig2 variable
                y= ["Alcohol", 'Alcohol_developed', 'Alcohol_developing'], #☻ y axis
                      labels = {'x':'Years', 'value' : 'Alcohol Consumption (liters of pure alcohol)'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected 
        
        return fig2 # We return the graph called fig2





 
############################################ Tab 6
############### Sub tab 34 (nineth sub tab of tab 6) Number of Worldwide Reported Cases having Measles in Developed Countries, Developing Countries and both at the same time between 2000 and 2015 (reported cases for 1000 habitants)

# Callback for Tab 6 and subtab 34 
@app.callback(Output('subtab34', 'children'), # Output (what we see) is the content of the nineth subtab in tab 6
              [Input('subtabs26', 'value'), # Input (what we give) is the content of the different subtabs that we choose in tab 6
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected

# Function to display a graph in the nineth subtab in tab 6
def display_graph_subtab34(value, toggle):
    
    if value == 'Subtab34': # If we are in subtab 34 in tab 6 we return a graph
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        
        return html.Div([
            html.Div(
            html.P("Number of Worldwide Reported Cases having Measles in Developed Countries, Developing Countries and both at the same time between 2000 and 2015 (reported cases for 1000 habitants)", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # style of graph title
            ),dcc.Graph(id="Mean_measles", # dcc.graph allows to make a Graph
                                   
                                   figure = px.line (mean_life_exp, x = 'Year', # store graph in figure variable
                                                     y= ['Measles','Measles_developed_countries', # y axis
                                                         'Measles_developing_countries'], 
                                                     labels = {'x':'Years', 'value' : 'Number of reported cases having measles'}, # x and y axis names
                                                     markers=True, # markers to have points on the line
                                                     template=template))]) # template selected

############################################ Tab 6
############### Sub tab 35 (tenth sub tab of tab 6) Number of Worldwide Reported Cases having Measles By Developed Countries between 2000 and 2015 (reported cases for 1000 habitants)

@app.callback(Output('subtab35', 'children'), # Output is the content of the tenth sub tab in tab 6
              [Input('subtabs26', 'value'),  # Input is the content of the different sub tabs that we choose in tab 6
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected

# Function to create a dropdown menu and radio items 
def dropdown_countries(value, toggle):  
    if value == 'Subtab35': # If we are in subtab35 in tab 6 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the developed country where you want to see the Number of Reported Cases having Measles between 2000 and 2015 (liters of pure alcohol)",  # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}),# style of text
                    dcc.Dropdown(id='worldwide_dropdown_measles', # Create dropdown (list with all countries to choose the one where we want to see the number of reported cases between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developed['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp_developed data frame
                    placeholder= 'Select the developed country',  # Placeholder, text that appears within the dropdown
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder 
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                    
                    dcc.Graph(id="scatter-plot-measles"),  # dcc.graph allows to create a graph
                    html.P("Number of Worldwide Reported Cases having Measles By Developed Countries between 2000 and 2015 (reported cases for 1000 habitants)", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}),# Style of graph title
                    ]) 
   


# Callback for Tab 6 and sub tab 35 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-measles", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_measles", "value")], # Input is the data from the list in the dropdown 
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected   


def update_graph_countries_subtab35(dropdown, radio_items, toggle): # Create a function to update the graph
    template = template_theme1 if toggle else template_theme2  # We define the theme selected 
    mask = (life_exp_developed.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp_developed data frame

    temp_life_exp_developed = life_exp_developed[mask]  # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developed["Measles_developed"] = list(mean_life_exp_developed.Measles) # create column Measles_developed
    temp_life_exp_developed["Measles_developing"] = list(mean_life_exp_developing.Measles) # create column Measles_developing

    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developed, x="Year", # store graph in fig variable
                y= ["Measles", 'Measles_developed', 'Measles_developing'], # y axis
                labels = {'x':'Years', 'value' : 'Number of reported cases having measles'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # template selected
                

        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developed, x="Year", # store graph in fig2 variable
                y= ["Measles", 'Measles_developed', 'Measles_developing'], # y axis
                      labels = {'x':'Years', 'value' : 'Number of reported cases having measles'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected
        
        return fig2 # We return the graph called fig2




############################################ Tab 7
############### Sub tab 36 (first sub tab of tab 7) Mean of the Worldwide Immunization Coverage among 1-year-olds By Developing Countries for Hepatitis B (between 2000-2015 in %)



# Callback for Tab 7 and subtab 36 
@app.callback(Output('subtab36', 'children'), # Output is the content of the first sub tab in tab 7
              [Input('subtabs36', 'value'), # Input is the content of the different sub tabs that we choose in tab 7
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")])  # Another input is the theme selected   

# Function to create a dropdown menu and radio items (different countries that we can choose to see their life expectancy and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab36': # If we are in subtab 36 in tab 7 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the developing country where you want to see the Immunization Coverage among 1-year-olds for Hepatitis B between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_hepatitis_developing', # Create dropdown (list with all countries to choose the one where we want to see the mean life expectancy between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developing['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp data frame
                    placeholder= 'Select the developing country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                    
                    dcc.Graph(id="scatter-plot-hepatitis-developing"),  # dcc.graph allows to create a graph
                    html.P("Mean of the Worldwide Immunization Coverage among 1-year-olds By Developing Countries for Hepatitis B (between 2000-2015 in %)", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}), # Style of graph title
                    ]) 
   
# Callback for Tab 7 and sub tab 36 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-hepatitis-developing", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_hepatitis_developing", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  


def update_graph_countries_subtab36(dropdown, radio_items, toggle): # Create a function to update the graph
    
    mask = (life_exp_developing.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    
    temp_life_exp_developing = life_exp_developing[mask] # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developing["Hepatitis_B_developed"] = list(mean_life_exp_developed.Hepatitis_B) # create column Hepatitis_B_developed
    temp_life_exp_developing["Hepatitis_B_developing"] = list(mean_life_exp_developing.Hepatitis_B) # create column Hepatitis_B_developing
    
    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developing, x = 'Year', # store graph in fig variable
                                                     y= ['Hepatitis_B','Hepatitis_B_developed', # y axis
                                                         'Hepatitis_B_developing'],
                labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # template selected
                

        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developing, x = 'Year', # store graph in fig2  variable
                                                     y= ['Hepatitis_B','Hepatitis_B_developed', # y axis
                                                         'Hepatitis_B_developing'],
                      labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template # template selected
) 
        
        return fig2 # We return the graph called fig2


  

############################################ Tab 7
############### Sub tab 37 (second sub tab of tab 7) Mean of the Worldwide Immunization Coverage among 1-year-olds By Developing Countries for Polio (between 2000-2015 in %)

# Callback for Tab 7 and subtab 37 
@app.callback(Output('subtab37', 'children'), # Output is the content of the second sub tab in tab 7
              [Input('subtabs36', 'value'), # Input is the content of the different sub tabs that we choose in tab 7
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  

# Function to create a dropdown menu and radio items (different countries that we can choose to see their life expectancy and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab37': # If we are in subtab37 in tab 7 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the developing country where you want to see the Immunization Coverage among 1-year-olds for Polio between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_Polio_developing', # Create dropdown (list with all countries to choose the one where we want to see the mean life expectancy between 2000 and 2015)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developing['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp data frame
                    placeholder= 'Select the developing country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                   
                    dcc.Graph(id="scatter-plot-Polio-developing"), # dcc.graph allows to create a graph
                    html.P("Mean of the Worldwide Immunization Coverage among 1-year-olds By Developing Countries for Polio (between 2000-2015 in %)", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # Style of graph title
                    ]) 
 
    
    
# Callback for Tab 7 and sub tab 37 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-Polio-developing", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_Polio_developing", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown
    [Input('radio_items_type_graph', 'value'),  # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  


def update_graph_countries_subtab37(dropdown, radio_items, toggle): # Create a function to update the graph
    
    mask = (life_exp_developing.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    temp_life_exp_developing = life_exp_developing[mask] # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developing["Polio_developed"] = list(temp_life_exp_developing.Polio) # create column Polio_developed
    temp_life_exp_developing["Polio_developing"] = list(temp_life_exp_developing.Polio) # create column Polio_developing

    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developing, x = 'Year', # store graph in fig variable
                                                     y= ['Polio','Polio_developed', # y axis
                                                         'Polio_developing'],
                labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # template selected
                

        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developing, x = 'Year',  # store graph in fig2 variable
                                                     y= ['Polio','Polio_developed', # y axis
                                                         'Polio_developing'],
                      labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                      barmode='group',  # to have multiple bars (one bar for each variable)
                      template=template) #  template selected 
        
        return fig2 # We return the graph called fig2
    


   
############################################ Tab 7
############### Sub tab 38 (third sub tab of tab 7) Mean of the Worldwide Immunization Coverage among 1-year-olds By Developing Countries for Diphteria (between 2000-2015 in %)

# Callback for Tab 7 and subtab 38 
@app.callback(Output('subtab38', 'children'), # Output is the content of the third sub tab in tab 7
              [Input('subtabs36', 'value'), # Input is the content of the different sub tabs that we choose in tab 7
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected    

# Function to create a dropdown menu and radio items (different countries that we can choose to see their life expectancy and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab38': # If we are in subtab38 in tab 7 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the developing country where you want to see the Immunization Coverage among 1-year-olds for Diphteria between 2000 and 2015", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), #â style of text
                    dcc.Dropdown(id='worldwide_dropdown_diphteria_developing', # Create dropdown (list with all countries to choose one)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developing['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp_developing data frame
                    placeholder= 'Select the developing country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                    
                    dcc.Graph(id="scatter-plot-diphteria-developing"), # dcc.graph allows to create a graph
                    html.P("Mean of the Worldwide Immunization Coverage among 1-year-olds By Developing Countries for Diphteria (between 2000-2015 in %)", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # Style of graph title
                    ]) 
   
# Callback for Tab 7 and sub tab 38 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-diphteria-developing", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_diphteria_developing", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  


def update_graph_countries_subtab38(dropdown, radio_items, toggle): # Create a function to update the graph
    
    mask = (life_exp_developing.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp data frame
    template = template_theme1 if toggle else template_theme2 # We define the theme selected
    temp_life_exp_developing = life_exp_developing[mask] # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developing["Diphteria_developed"] = list(temp_life_exp_developing.Diphteria) # create column Diphteria_developed
    temp_life_exp_developing["Diphteria_developing"] = list(temp_life_exp_developing.Diphteria) # create column Diphteria_developing

    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developing, x = 'Year', # store graph in fig variable 
                                                     y= ['Diphteria','Diphteria_developed', # y axis
                                                         'Diphteria_developing'],
                labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # template selected
                

        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developing, x = 'Year', # store graph in fig2 variable
                                                     y= ['Diphteria','Diphteria_developed', # y axis
                                                         'Diphteria_developing'],
                      labels = {'x':'Years', 'value' : 'Immunization Coverage (%)'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected 
        
        return fig2 # We return the graph called fig2




############################################ Tab 7
############### Sub tab 39 (fourth sub tab of tab 7) Mean of the Worldwide Alcohol Consumption By Developing Countries between 2000 and 2015 (liters of pure alcohol)

# Callback for Tab 7 and subtab 39
@app.callback(Output('subtab39', 'children'), # Output is the content of the fourth sub tab in tab 7
              [Input('subtabs36', 'value'),  # Input is the content of the different sub tabs that we choose in tab 7
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected    

# Function to create a dropdown menu and radio items (different countries that we can choose and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab39': # If we are in subtab39 in tab 1=7 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # Create graph called scatter-plot
                    html.P("Choose the developing country where you want to see the Alcohol Consumption between 2000 and 2015 (liters of pure alcohol)", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), #○ style of text
                    dcc.Dropdown(id='worldwide_dropdown_alcohol_developing', # Create dropdown (list with all countries to choose one)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developing['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp data frame
                    placeholder= 'Select the developing country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                   
                    dcc.Graph(id="scatter-plot-alcohol-developing"), # dcc.graph allows to create a graph
                     html.P("Mean of the Worldwide Alcohol Consumption By Developing Countries between 2000 and 2015 (liters of pure alcohol)", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'})# Style of graph title
                    ]) 
   
# Callback for Tab 7 and sub tab 39 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-alcohol-developing", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_alcohol_developing", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  


def update_graph_countries_subtab39(dropdown, radio_items, toggle): # Create a function to update the graph
    template = template_theme1 if toggle else template_theme2 # We define the theme selected   
    mask = (life_exp_developing.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp_developing data frame
    temp_life_exp_developing = life_exp_developing[mask] # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developing["Alcohol_developed"] = list(mean_life_exp_developed.Alcohol) # create column Alcohol_developed
    temp_life_exp_developing["Alcohol_developing"] = list(mean_life_exp_developing.Alcohol) # create column Alcohol_developing

    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developing,x = "Year", # store graph in fig variable
                y= ["Alcohol", 'Alcohol_developed', 'Alcohol_developing'], # y axis
                labels = {'x':'Years', 'value' : 'Alcohol Consumption (liters of pure alcohol)'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # template selected
                

        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developing,x = "Year", # store graph in fig2 variable
                y= ["Alcohol", 'Alcohol_developed', 'Alcohol_developing'], # y axis
                      labels = {'x':'Years', 'value' : 'Alcohol Consumption (liters of pure alcohol)'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected 
        
        return fig2 # We return the graph called fig2
    





############################################ Tab 7
############### Sub tab 40 (fifth sub tab of tab 7) Number of Worldwide Reported Cases having Measles By Developing Countries between 2000 and 2015 (reported cases for 1000 habitants)

# Callback for Tab 7 and subtab 40
@app.callback(Output('subtab40', 'children'), # Output is the content of the fifth sub tab in tab 7
              [Input('subtabs36', 'value'), # Input is the content of the different sub tabs that we choose in tab 7
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected    

# Function to create a dropdown menu and radio items (different countries that we can choose to see their life expectancy and radioitem to choose between line chart or bar chart)
def dropdown_countries(value, toggle):  
    if value == 'Subtab40': # If we are in subtab2 in tab 1 we return a dropdown and radio items
        template = template_theme1 if toggle else template_theme2 # We define the theme selected

        return html.Div([ # html components
                    html.P("Choose the developing country where you want to see the Number of Reported Cases having Measles between 2000 and 2015 (liters of pure alcohol)", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.Dropdown(id='worldwide_dropdown_measles_developing', # Create dropdown (list with all countries to choose one)
                    options=[ # Define the options of the dropdown
                             {'label': countries, 'value': countries} for countries in life_exp_developing['Country'].unique()],# the dropdown list contains each unique countries that are stored in the column Country of life_exp_developing data frame
                    placeholder= 'Select the developing country', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of placeholder
                    
                    html.P("Choose the type of graph that you want to see", # Another sentence above the radio item
                           style={'textAlign': 'center','color': 'black', 'font-size':'100','font-family':'cursive'}), # style of text
                    dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                   options=[ # Definition of the options  within the radio items
                                       {'label': 'Line chart', 'value':'line'}, # First radio item with label Line chart
                                       {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                   value='bar', # Default value when we open the app/ sub tab
                                   style = {'text-align': 'center', 'color': 'black'}), # style of text
                    
                    dcc.Graph(id="scatter-plot-measles-developing"), # dcc.graph allows to create a graph
                    html.P("Number of Worldwide Reported Cases having Measles By Developing Countries between 2000 and 2015 (reported cases for 1000 habitants)", # graph title
                   style={'textAlign': 'center','color': 'black', 'font-size':'2em','font-family':'cursive'}) # Style of graph title
                    ]) 
   
# Callback for Tab 7 and sub tab 40 to display the graph (by countries)
@app.callback(
    Output("scatter-plot-measles-developing", "figure"), # Output is the scatter-plot
    [Input("worldwide_dropdown_measles_developing", "value")], # Input is the data from the list in the dropdown called worldwide_dropdown
    [Input('radio_items_type_graph', 'value'), # Another input is the selection of the radio item (line chart or bar chart)
               Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the theme selected  


def update_graph_countries_subtab40(dropdown, radio_items, toggle): # Create a function to update the graph
    template = template_theme1 if toggle else template_theme2 # We define the theme selected   
    mask = (life_exp_developing.Country == dropdown) # The item selected in the dropdown corresponds to the value of countries that are in the column Country of life_exp data frame

    temp_life_exp_developing = life_exp_developing[mask] # dataframe that contains the data for the specific country selected (mask variable)
    temp_life_exp_developing["Measles_developed"] = list(mean_life_exp_developed.Measles) # create column Measles_developed
    temp_life_exp_developing["Measles_developing"] = list(mean_life_exp_developing.Measles) # create column Measles_developing

    if radio_items =='line': # If the user chooses the radio item line we return a line plot
        fig = px.line(temp_life_exp_developing, x="Year", # store graph in fig variable
                y= ["Measles", 'Measles_developed', 'Measles_developing'], # y axis
                labels = {'x':'Years', 'value' : 'Number of reported cases having measles'}, # x and y axis names
                markers=True, # markers to have points on the line
                template=template) # template selected
                

        return fig # We return the graph called fig
    
    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar plot
        fig2 = px.bar(temp_life_exp_developing, x="Year", # store graph in fig2 variable
                y= ["Measles", 'Measles_developed', 'Measles_developing'], # y axis
                      labels = {'x':'Years', 'value' : 'Number of reported cases having measles'}, # x and y axis names
                      barmode='group', # to have multiple bars (one bar for each variable)
                      template=template) # template selected
        
        return fig2 # We return the graph called fig2
    


    

# Run the app
if __name__ =='__main__':
    app.run_server(debug=False)      

                                                                              
    
  
