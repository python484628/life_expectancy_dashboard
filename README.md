# Python_Dashboard

## Files provided

You have access to both files that contain the data used for the dashboard (Continent.csv and Life-Expectancy-Data.csv).

And you have access to the code that allows to run the app (Python_Dashboard_final.py).

## What's in the dashboard

You have access to a dashboard called "Life expectancy dashboard" with 7 tabs in which there are subtabs. You can change the main theme in order to have colors more or less light (moon and sun icons). You can also change the type of graph in some subtabs (bar chart or line chart). When you have multiple variables presented in a graph, you can select or unselect them in the area of the legend to make them appear or not in the graph.

## The first tab is called "Worldwide life expectancy" and it has 4 subtabs : 

1- Life Expectancy Worldwide, showing the mean life expectancy worldwide by years between 2000 and 2015 ; 

2- Life Expectancy Worldwide By Countries, showing the mean life expectancy by selected countries (you have a drop-down list in which you can choose the country where you want to see the mean life expectancy between 2000 and 2015) ;

3- Life Expectancy Worldwide By Continents, showing the mean life expectancy by selected continents (you have a drop-down list in which you can choose the continent where you want to see the mean life expectancy between 2000 and 2015) ;

4- Life Expectancy Worldwide (Map), showing a map of the mean life expectancy worldwide by years between 2000 and 2015.


## The second tab is called "Worldwide mortality" and it has 5 subtabs :

1- Adult Death and Infant death Worldwide, showing the mean of infant and adult death worldwide (mean of each country that we had between 2000 and 2015) ;

2- Adult Death and Infant death By Countries, showing the mean of infant and adult death by selected countries between 2000 and 2015 ;

3- Adult Death and Infant death By Continents, showing the mean of infant and adult death by selected continents between 2000 and 2015 ;

4- Adult Death (Map), showing a map of the mean adult death worldwide between 2000 and 2015 ; 

5- Infant Death (Map), showing a map of the mean infant death worldwide between 2000 and 2015.


## The third tab is called "Worldwide health" and it has 9 subtabs :

1- Worldwide Immunization Coverage, showing the mean immunization coverage worldwide among 1 year old children concerning hepatitis b, polio and diphteria by years between 2000 and 2015 ;

2- Worldwide Immunization Coverage By Countries, showing the mean immunization coverage worldwide among 1 year old children concerning hepatitis b, polio and diphteria by selected countries between 2000 and 2015. We have also put the mean immunization coverage for developed and deloping countries in order to situate the selected country compared to those developed and developing countries.

3- Worldwide Immunization Coverage By Continents, showing the mean immunization coverage worldwide among 1 year old children concerning hepatitis b, polio and diphteria by selected continents between 2000 and 2015. 

4- Worldwide Alcohol Consumption, showing the mean alcohol consumption worldwide by years between 2000 and 2015 (in liters of pure alcohol) ;

5- Worldwide Alcohol Consumption By Countries, showing the mean alcohol consumption in the selected countries by years between 2000 and 2015 (in liters of pure alcohol). We have also put the mean of alcohol consumption in the developed and developing countries in order to situate ourselves ;

6- Worldwide Alcohol Consumption By Continents, showing the mean alcohol consumption in the selected continents by years between 2000 and 2015 (in liters of pure alcohol) ;

7- Worldwide Reported Cases having Measles, showing the number of people having measles (number of reported cases) worldwide by years between 2000 and 2015 ;

8- Worldwide Reported Cases having Measles By Countries, showing the number of people having measles (number of reported cases) in the selected countries by years between 2000 and 2015 ;

9- Worldwide Reported Cases having Measles By Continents, showing the number of people having measles (number of reported cases) in the selected continents by years between 2000 and 2015 ; 


## The fourth tab is called "Worldwide life expectancy between developed and developing countries" and it has 3 subtabs :

1- Life Expectancy Worldwide, showing the mean life expectancy worldwide by years between 2000 and 2015 for developed countries, developing countries, and both at the same time ;

2- Life Expectancy Worldwide By Developed Countries, showing the mean life expectancy in the selected developed countries by years between 2000 and 2015. We have also put the mean life expectancy in all developed countries and developing countries in order to situate ourselves ;

3- Life Expectancy Worldwide By Developing Countries, showing the mean life expectancy in the selected developing countries by years between 2000 and 2015. We have also put the mean life expectancy in all developed countries and developing countries in order to situate ourselves.

## The fifth tab is called "Worldwide mortality between developed and developing countries" and it has 6 subtabs :

1- Mean Adult Death Worldwide, showing the adult mortality worldwide for developed countries, developing countries, and both at the same time by years between 2000 and 2015 ;

2- Mean Adult Death Worldwide By Developed Countries, showing the mean adult mortality in the selected developed countries by years between 2000 and 2015. We have also put the adult mortality in all developed countries and developing countries in order to situate ourselves ;

3- Mean Adult Death Worldwide By Developing Countries, showing the mean adult mortality in the selected developing countries by years between 2000 and 2015. We have also put the mean adult mortality in developed countries, developing countries, and both at the same time in order to situate ourselves ;

4- Mean Infant Death Worldwide, showing the mean infant mortality worldwide for developed countries, developing countries, and both at the same time by years between 2000 and 2015 ;

5- Mean Infant Death Worldwide By Developed Countries, showing the mean infant mortality in the selected developed countries by years between 2000 and 2015. We have also put the mean infant mortality in developed countries and developing countries in order to situate ourselves ;

6- Mean Infant Death Worldwide By Developing Countries, showing the mean infant mortality in the selected developing countries by years between 2000 and 2015. We have also put the mean infant mortality in developed countries and developing countries in order to situate ourselves ;


## The sixth tab is called "Worldwide health for Developed Countries" and it has 10 subtabs :

1- Worldwide Immunization Coverage (hepatitis b), showing the mean immunization coverage among 1 year old children concerning hepatitis b in developed countries, developing countries, and both at the same time by years between 2000 and 2015 ;

2- Worldwide Immunization Coverage By Developed Countries (hepatitis b), showing the mean immunization coverage among 1 year old children concerning hepatitis b in the selected developed countries by years between 2000 and 2015. We have also put the mean immunization coverage in developed countries and developing countries in order to situate ourselves ;

3- Worldwide Immunization Coverage (polio), showing the mean immunization coverage among 1 year old children concerning polio in developed countries, developing countries, and both at the same time by years between 2000 and 2015 ;

4- Worldwide Immunization Coverage By Developed Countries (polio), showing the mean immunization coverage among 1 year old children concerning polio in the selected developed countries by years between 2000 and 2015. We have also put the mean immunization coverage in developed countries and developing countries in order to situate ourselves ;

5- Worldwide Immunization Coverage (diphteria), showing the mean immunization coverage among 1 year old children concerning diphteria in developed countries, developing countries, and both at the same time by years between 2000 and 2015 ;

6- Worldwide Immunization Coverage By Developed Countries (diphteria), showing the mean immunization coverage among 1 year old children concerning diphteria in the selected developed countries by years between 2000 and 2015. We have also put the mean immunization coverage in developed countries and developing countries in order to situate ourselves ;

7- Worldwide Alcohol Consumption, showing the mean alcohol consumption (liters of pure alcohol) in developed countries, developing countries, and both at the same time by years between 2000 and 2015 ;

8- Worldwide Alcohol Consumption By Developed Countries, showing the mean alcohol consumption (liters of pure alcohol) in the selected developed countries by years between 2000 and 2015. We have also put the mean alcohol consumption in developed countries and developing countries in order to situate ourselves ;

9- Worldwide Reported Cases having Measles, showing the number of people having measles (number of reported cases) in developed countries, developing countries, and both at the same time by years between 2000 and 2015 ;

10- Worldwide Reported Cases having Measles By Developed Countries, showing the number of people having measles (number of reported cases) in the selected developed countries by years between 2000 and 2015. We have also put the number of reported cases having measles in developed countries and developing countries in order to situate ourselves.


## The seventh tab is called "Worldwide health for Developing Countries" and it has 5 subtabs :

1- Worldwide Immunization Coverage By Developing Countries (hepatitis b), showing the mean immunization coverage among 1 year old 

2- Worldwide Immunization Coverage By Developing Countries (polio), showing the mean immunization coverage among 1 year old children concerning polio in the selected developing countries by years between 2000 and 2015. We have also put the mean immunization coverage in developed countries and developing countries in order to situate ourselves ;

3- Worldwide Immunization Coverage By Developing Countries (diphteria), showing the mean immunization coverage among 1 year old children concerning diphteria in the selected developing countries by years between 2000 and 2015. We have also put the mean immunization coverage in developed countries and developing countries in order to situate ourselves ;

4- Worldwide Alcohol Consumption By Developing Countries, showing the mean alcohol consumption (liters of pure alcohol) in the selected developing countries by years between 2000 and 2015. We have also put the mean alcohol consumption in developed countries and developing countries in order to situate ourselves ;

5- Worldwide Reported Cases having Measles By Developing Countries, showing the number of people having measles (number of reported cases) in the selected developing countries by years between 2000 and 2015. We have also put the number of reported cases having measles in developed countries and developing countries in order to situate ourselves.




Authors : Marion Estoup and Solal Peiffer-Smadja

E-mail : marion_110@hotmail.fr

January 2021







